# Pipeline

This folder contains the sources of the pipeline.

## Requirements

An installed _openjdk_ Version 8 or greater. You can get it for

1. Linux: Install _openjdk_ with your package manager
2. macOS: Install it with homebrew: `brew install openjdk`
3. Windows: Install it with the provided [installer](https://www.microsoft.com/openjdk)

Also [SCIP](https://www.scipopt.org/) >= 7.0.0 and the adjusted version of the [NashFlowComputationTool](https://github.com/maxemann96/NashFlowComputation/tree/cli) must be installed. To get an idea how to setup locally, checkout the `Dockerfile`.

## Setup

```sh
./gradlew clean assemble
```

## Built for production

```sh
./gradlew installDist
```

## Run

Use the play button provided by the IDE or

```sh
./gradlew run
```

On default configuration, it generates a 4x4 network and calculates ten traffic lights with random capacities and the greedy heuristic.
