plugins {
    val kotlinVersion = "1.6.0"

    kotlin("jvm") version kotlinVersion
    kotlin("plugin.serialization") version kotlinVersion

    application
}

group = "dev.maximilian.tlo"
version = "1.0.0-SNAPSHOT"

repositories {
    mavenCentral()
}

val javaVersion: JavaVersion = JavaVersion.VERSION_11
val exposedVersion: String by project
val slf4jVersion: String by project
val postgresVersion: String by project
val kotlinxSerializationVersion: String by project
val jGraphTVersion: String by project
val muVersion: String by project

dependencies {
    // Align versions of all Kotlin components
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))

    // Use the Kotlin JDK 8 standard library.
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // Database framework with postgres driver
    implementation("org.jetbrains.exposed", "exposed-core", exposedVersion)
    implementation("org.jetbrains.exposed", "exposed-dao", exposedVersion)
    implementation("org.jetbrains.exposed", "exposed-jdbc", exposedVersion)
    implementation("org.postgresql", "postgresql", postgresVersion)

    // Json (De-)Serializer
    implementation("org.jetbrains.kotlinx", "kotlinx-serialization-core", kotlinxSerializationVersion)
    implementation("org.jetbrains.kotlinx", "kotlinx-serialization-json", kotlinxSerializationVersion)

    // Graph library
    implementation("org.jgrapht", "jgrapht-core", jGraphTVersion)
    implementation("org.jgrapht", "jgrapht-ext", jGraphTVersion)

    // Linear optimizer
    implementation("org.ojalgo", "ojalgo", "48.4.2")

    // Logging with simple logging at runtime
    implementation("org.slf4j", "slf4j-api", slf4jVersion)
    implementation("org.slf4j", "slf4j-simple", slf4jVersion)
    implementation("io.github.microutils", "kotlin-logging-jvm", muVersion)

    // Kotlin test
    testImplementation(kotlin("test"))

    // Im memory database
    testImplementation("com.h2database", "h2", "1.4.200")
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "$javaVersion"
    }

    compileTestKotlin {
        kotlinOptions.jvmTarget = "$javaVersion"
    }

    java {
        sourceCompatibility = javaVersion
        targetCompatibility = javaVersion
    }

    test {
        useJUnitPlatform()
    }

    application {
        applicationName = "tlo"
        mainClass.set("dev.maximilian.tlo.MainKt")
    }

    distributions {
        main {
            distributionBaseName.set("tlo")
        }
    }
}
