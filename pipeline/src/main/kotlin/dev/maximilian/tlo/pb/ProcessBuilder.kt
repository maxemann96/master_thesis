package dev.maximilian.tlo.pb

import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.SerializationStrategy
import kotlinx.serialization.json.Json
import kotlinx.serialization.serializer
import mu.KotlinLogging
import java.io.BufferedReader
import java.io.InputStreamReader

public class ProcessBuilder(private vararg val command: String, private val json: Json = Json) {
    private val logger = KotlinLogging.logger { }

    init {
        require(command.isNotEmpty()) { "At least one command must be given" }
    }

    public fun execute(stdIn: String): String {
        val pb = java.lang.ProcessBuilder(*command)
        val proc = pb.start()

        proc.outputStream.apply {
            write(stdIn.encodeToByteArray())
            logger.debug { stdIn }
            flush()
            close()
        }

        val output = BufferedReader(InputStreamReader(proc.inputStream)).readText()
        val error = BufferedReader(InputStreamReader(proc.errorStream)).readText()

        if (error.isNotBlank()) {
            throw IllegalStateException("\"${command[0]}\" Error: $error")
        }

        return output
    }

    public fun <Input, Output> execute(
        serializer: SerializationStrategy<Input>,
        stdIn: Input,
        deserializer: DeserializationStrategy<Output>
    ): Output =
        json.decodeFromString(deserializer, execute(json.encodeToString(serializer, stdIn)))

    public inline fun <reified Input, Output> execute(
        stdIn: Input,
        deserializer: DeserializationStrategy<Output>
    ): Output = execute(serializer(), stdIn, deserializer)

    public inline fun <Input, reified Output> execute(serializer: SerializationStrategy<Input>, stdIn: Input): Output =
        execute(serializer, stdIn, serializer())

    public inline fun <reified Input, reified Output> execute(stdIn: Input): Output =
        execute(serializer(), stdIn, serializer())
}
