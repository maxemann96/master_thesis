package dev.maximilian.tlo.format

import dev.maximilian.tlo.SerializableEdge
import dev.maximilian.tlo.SerializableGraph
import dev.maximilian.tlo.SerializableNode
import kotlinx.serialization.KSerializer
import kotlinx.serialization.builtins.MapSerializer
import kotlinx.serialization.builtins.SetSerializer
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonNull
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonTransformingSerializer
import kotlinx.serialization.json.buildJsonArray
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put
import kotlinx.serialization.serializer

inline fun <reified GraphMetadata, reified NodeMetadata, reified EdgeMetadata> cytoscapeSerializer():
    KSerializer<SerializableGraph<GraphMetadata, NodeMetadata, EdgeMetadata>> =
    CytoscapeSerializer(serializer(), serializer(), serializer())

class CytoscapeSerializer<GraphMetadata, NodeMetadata, EdgeMetadata>(
    graphSerializer: KSerializer<SerializableGraph<GraphMetadata, NodeMetadata, EdgeMetadata>>,
    nodeSerializer: KSerializer<SerializableNode<NodeMetadata>>,
    edgeSerializer: KSerializer<SerializableEdge<EdgeMetadata>>
) : JsonTransformingSerializer<SerializableGraph<GraphMetadata, NodeMetadata, EdgeMetadata>>(graphSerializer) {
    private val nodeTransformSerializer = CytoscapeNodeSerializer(nodeSerializer)
    private val edgeTransformSerializer = CytoscapeEdgeSerializer(edgeSerializer)

    override fun transformSerialize(element: JsonElement): JsonElement {
        require(element is JsonObject) { "Graph was not transformed to a json object" }

        val metaData = element["metadata"] ?: JsonNull

        require(metaData is JsonObject || metaData is JsonNull) { "Graph metadata was not transformed to a json object" }

        val originalKeys = element.keys.filter { it != "metadata" }
        val metaDataKeys = (metaData as? JsonObject)?.keys ?: emptySet()

        require(metaDataKeys.intersect((originalKeys + "elements" - "nodes" - "edges").toSet()).isEmpty()) { "Graph metadata contains key(s) of the graph object" }

        return buildJsonObject {
            element.entries.filter { it.key != "metadata" && it.key != "nodes" && it.key != "edges" }.forEach {
                put(it.key, it.value)
            }

            put(
                "elements",
                buildJsonObject {
                    val originalNodes = element["nodes"] ?: JsonNull
                    require(originalNodes is JsonObject || originalNodes is JsonNull) { "Nodes of the graph needs to be a json object" }
                    put("nodes", nodeTransformSerializer.transformSerialize(originalNodes))

                    val originalEdges = element["edges"] ?: JsonNull
                    require(originalEdges is JsonArray || originalEdges is JsonNull) { "Edges of the graph needs to be a json array" }
                    put("edges", edgeTransformSerializer.transformSerialize(originalEdges))
                }
            )

            put("data", JsonObject((metaData as? JsonObject) ?: emptyMap()))
            put("directed", true)
        }
    }
}

class CytoscapeEdgeSerializer<EdgeMetadata>(ser: KSerializer<SerializableEdge<EdgeMetadata>>) :
    JsonTransformingSerializer<Set<SerializableEdge<EdgeMetadata>>>(SetSerializer(ser)) {
    public override fun transformSerialize(element: JsonElement): JsonElement {
        require(element is JsonArray) { "Edge set was not transformed to json array" }

        return buildJsonArray {
            element.forEach { el ->
                require(el is JsonObject) { "Edge was not transformed to a json object" }

                val metaData = el["metadata"] ?: JsonNull

                require(metaData is JsonObject || metaData is JsonNull) { "Edge metadata was not transformed to a json object" }

                val originalKeys = el.keys.filter { it != "metadata" }
                val metaDataKeys = (metaData as? JsonObject)?.keys ?: emptySet()

                require(metaDataKeys.intersect(originalKeys.toSet()).isEmpty()) { "Edge metadata contains key(s) of the edge object" }

                add(
                    buildJsonObject {
                        put(
                            "data",
                            buildJsonObject {
                                el.entries.filter { it.key != "metadata" }.forEach {
                                    put(it.key, it.value)
                                }

                                (metaData as? JsonObject)?.forEach {
                                    put(it.key, it.value)
                                }
                            }
                        )
                    }
                )
            }
        }
    }
}

class CytoscapeNodeSerializer<NodeMetadata>(ser: KSerializer<SerializableNode<NodeMetadata>>) :
    JsonTransformingSerializer<Map<String, SerializableNode<NodeMetadata>>>(MapSerializer(String.serializer(), ser)) {
    public override fun transformSerialize(element: JsonElement): JsonElement {
        require(element is JsonObject) { "Node list was not transformed to a json object" }

        val objects = element.entries.map { (id, el) ->
            require(el is JsonObject) { "Node \"$id\" was not transformed to a json object" }

            val metaData = el["metadata"] ?: JsonNull

            require(metaData is JsonObject || metaData is JsonNull) { "Node \"$id\" metadata was not transformed to a json object" }

            val originalKeys = el.keys.filter { it != "metadata" }
            val metaDataKeys = (metaData as? JsonObject)?.keys ?: emptySet()

            require(
                metaDataKeys.intersect((originalKeys + "value").toSet()).isEmpty()
            ) { "Node \"$id\" metadata contains key(s) of the node object" }

            buildJsonObject {
                put(
                    "data",
                    buildJsonObject {
                        put("id", id)
                        put("value", id)

                        el.entries.filter { it.key != "metadata" && it.key != "id" }.forEach {
                            put(it.key, it.value)
                        }

                        (metaData as? JsonObject)?.forEach {
                            put(it.key, it.value)
                        }
                    }
                )
            }
        }

        return buildJsonArray {
            objects.forEach { add(it) }
        }
    }
}
