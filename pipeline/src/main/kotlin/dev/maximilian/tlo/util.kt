package dev.maximilian.tlo

import org.jgrapht.graph.DirectedWeightedMultigraph
import java.security.MessageDigest

fun Iterable<Double>.isSorted() = this.asSequence().zipWithNext { a, b -> a <= b }.all { it }

private fun ByteArray.toHex(): String = joinToString(separator = "") { eachByte -> "%02x".format(eachByte) }

fun String.sha256Sum() = MessageDigest.getInstance("SHA3-256").digest(this.toByteArray()).toHex()

fun DirectedWeightedMultigraph<Node, Edge>.addEdge(e: Edge) {
    require(addEdge(e.source, e.target, e)) { "Error while adding edge to graph" }
    setEdgeWeight(e, e.transitTime)
}
