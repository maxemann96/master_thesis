package dev.maximilian.tlo

import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.booleanOrNull
import kotlinx.serialization.json.doubleOrNull
import org.jgrapht.graph.DefaultGraphType
import org.jgrapht.graph.DirectedWeightedMultigraph
import org.jgrapht.graph.builder.GraphTypeBuilder

data class Node(
    val id: String,
    val label: String?,
    val originalId: String = id,
    val trafficLight: Boolean
)

data class Edge(
    val id: String,
    val transitTime: Double,
    val capacity: Double,
    val source: Node,
    val target: Node,
    val label: String? = null
) {
    init {
//        require(transitTime >= 0) { "Transit time must be greater or equal 0"}
        require(capacity >= 0) { "Edge capacity must be greater or equal 0" }
    }
}

data class BipartiteTrafficLightGadget(
    val oldNode: Node,
    val newNodesL: Set<Node>,
    val newNodesR: Set<Node>,
    val bipartiteEdges: Set<Edge>
)

enum class TransformationType {
    NOTHING, TRAFFIC_LIGHT_RANDOM, TRAFFIC_LIGHT_SHORTEST_PATH, OPTIMIZE
}

data class GraphTransformation(
    val id: Int,
    val type: TransformationType,
    val description: String,
    val payload: String
) {
    init {
        require(description.length <= 255) { "Description can have 255 characters at maximum" }
    }
}

internal fun SerializableGraphBase<JsonObject, JsonObject, JsonObject>.toGraphs(): List<Triple<DirectedWeightedMultigraph<Node, Edge>, Set<Node>, Set<Edge>>> =
    graph?.toGraph()?.let { listOf(it) } ?: graphs?.map { it.toGraph() }
        ?: throw IllegalStateException("SerializableGraphBase should ensure that graph or graphs is given")

internal fun SerializableGraph<JsonObject, JsonObject, JsonObject>.toGraph(): Triple<DirectedWeightedMultigraph<Node, Edge>, Set<Node>, Set<Edge>> =
    (
        GraphTypeBuilder.forGraphType<Node, Edge>(DefaultGraphType.dag().asWeighted())
            .buildGraph() as DirectedWeightedMultigraph<Node, Edge>
        ).let {
        val nodes: Set<Node> = nodes?.entries?.map { (id, n) -> n.toNode(id) }?.toSet() ?: emptySet()
        val edges: Set<Edge> = edges?.map { e -> e.toEdge(nodes.associateBy { n -> n.id }) }?.toSet() ?: emptySet()

        nodes.forEach { n -> it.addVertex(n) }
        edges.forEach { e -> it.addEdge(e.source, e.target, e) }

        Triple(it, nodes, edges)
    }

internal fun SerializableNode<JsonObject>.toNode(id: String) = Node(
    id = id,
    originalId = id,
    label = label,
    trafficLight = metadata?.let {
        val trafficLightElement = it["trafficLight"]

        if (trafficLightElement != null) {
            require(trafficLightElement is JsonPrimitive) {
                "trafficLight of node $id is specified, but cannot be interpreted as json primitive"
            }

            val trafficLight = trafficLightElement.booleanOrNull

            requireNotNull(trafficLight) {
                "trafficLight of node $id is specified, but cannot be interpreted as Boolean, is: \"${trafficLightElement.content}\""
            }

            trafficLight
        } else false
    } ?: false
)

internal fun SerializableEdge<JsonObject>.toEdge(nodes: Map<String, Node>): Edge {
    val source: Node? = nodes[source]
    val target: Node? = nodes[target]
    val metadata: JsonObject? = metadata

    requireNotNull(source) { "Source node $source not found in node set" }
    requireNotNull(target) { "Target node $target not found in node set" }
    requireNotNull(metadata) { "Metadata information of edge ${id ?: "($source, $target)"} not found" }

    val capacityElement: JsonElement? = metadata["outCapacity"]
    val transitTimeElement: JsonElement? = metadata["transitTime"]

    require(capacityElement is JsonPrimitive) {
        "Metadata information of edge ${id ?: "(${source.id}, ${target.id})"} malformed. Capacity is not a json primitive"
    }

    require(transitTimeElement is JsonPrimitive) {
        "Metadata information of edge ${id ?: "(${source.id}, ${target.id})"} malformed. TransitTime is not a json primitive"
    }

    val capacity = capacityElement.doubleOrNull
    val transitTime = transitTimeElement.doubleOrNull

    requireNotNull(capacity) {
        "Metadata information of edge ${id ?: "(${source.id}, ${target.id})"} malformed. Capacity is not a double"
    }

    requireNotNull(transitTime) {
        "Metadata information of edge ${id ?: "(${source.id}, ${target.id})"} malformed. TransitTime is not a double"
    }

    return Edge(
        id = id ?: "(${source.id},${target.id})",
        source = source,
        target = target,
        label = label,
        capacity = capacity,
        transitTime = transitTime
    )
}
