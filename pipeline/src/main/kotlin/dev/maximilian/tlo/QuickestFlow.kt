package dev.maximilian.tlo

import mu.KotlinLogging
import org.jgrapht.GraphPath
import org.jgrapht.alg.shortestpath.BellmanFordShortestPath
import org.jgrapht.alg.util.ToleranceDoubleComparator
import org.jgrapht.graph.DefaultGraphType
import org.jgrapht.graph.builder.GraphTypeBuilder
import org.ojalgo.optimisation.ExpressionsBasedModel
import org.ojalgo.optimisation.Optimisation
import org.ojalgo.optimisation.Variable
import java.util.UUID
import kotlin.math.abs
import kotlin.math.round

class QuickestFlow(
    public val graph: SingleSourceTargetGraph,
    public val flow: Double,
    public val inflowRate: Double
) {
    private companion object {
        private const val QF_ROUND_TO: Double = 1e5
    }

    private val logger = KotlinLogging.logger {}

    init {
        require(flow >= 0.0) { "Can only do a quickest flow with a flow value greater or equal 0" }
    }

    public val timeHorizon: Double by lazy { result.first }

    public val flowDecomposition: List<Pair<List<Node>, Double>> by lazy { result.second }

    private val result: Pair<Double, List<Pair<List<Node>, Double>>> by lazy { calculateResult() }

    private fun calculateResult(): Pair<Double, List<Pair<List<Node>, Double>>> {
        var lower = 0.0
        val shortestPath = BellmanFordShortestPath(graph.graph).getPath(graph.source, graph.target)
        var upper = flow / shortestPath.edgeList.minOf { it.capacity } + shortestPath.edgeList.sumOf { it.transitTime }
        var lastValue = 0.0
        var lastFlowDecomposition: List<Pair<GraphPath<Node, Edge>, Double>> = emptyList()
        var iterations = 0
        var middle = upper / 2.0

        logger.debug { "Searching quickest flow with value $flow in [$lower, $upper]" }

        // After 64 iterations the precision should already be there
        // This ensures that this loop terminates in case of some double precision is causing trouble
        // TODO this number was chosen arbitrary because it should be good enough. Think about it again please!
        while (abs(lastValue - flow) > ToleranceDoubleComparator.DEFAULT_EPSILON && iterations < 64) {
            middle = (upper + lower) / 2.0
            logger.trace { "Searching Quickest flow between $lower and $upper" }
            val minCostSolution = solveMinCostCirulationProblem(middle, inflowRate)
            lastFlowDecomposition = calculateFlowDecomposition(minCostSolution)
            val flowValue = lastFlowDecomposition.sumOf { it.second * (middle - it.first.edgeList.sumOf { e -> e.transitTime }) }

            logger.trace {
                "[${lower.toString().padEnd(20, '0')}, ${upper.toString().padEnd(20, '0')}], ${
                middle.toString().padEnd(20, '0')
                }: ${flowValue.toString().padEnd(20, '0')}"
            }

            if (flowValue <= flow) {
                lower = middle
            } else {
                upper = middle
            }

            lastValue = flowValue
            iterations++
        }

        return round(middle * QF_ROUND_TO) / QF_ROUND_TO to lastFlowDecomposition.map { it.first.vertexList to it.second }
    }

    private fun calculateFlowDecomposition(flowMap: Map<Edge, Double>): List<Pair<GraphPath<Node, Edge>, Double>> {
        val flowMapCopy: MutableMap<Edge, Double> = flowMap.toMutableMap()
        var flowValue = flowMapCopy.filter { it.key.target == graph.target }.values.sum()
        val paths: MutableList<Pair<GraphPath<Node, Edge>, Double>> = mutableListOf()

        // >0, but we are working with doubles, so we need a small epsilon
        while (flowValue > ToleranceDoubleComparator.DEFAULT_EPSILON) {
            val graphCopy = GraphTypeBuilder.forGraphType<Node, Edge>(DefaultGraphType.dag().asWeighted()).buildGraph()
            flowMapCopy.keys.map { listOf(it.source, it.target) }.flatten().toSet().forEach {
                graphCopy.addVertex(it)
            }

            flowMapCopy.filter { it.value > ToleranceDoubleComparator.DEFAULT_EPSILON }.keys.forEach { e ->
                graphCopy.addEdge(e.source, e.target, e)
                graphCopy.setEdgeWeight(e, e.transitTime)
            }

            val path = BellmanFordShortestPath(graphCopy).getPath(graph.source, graph.target)
            val pathValue: Double = path.edgeList.map { flowMapCopy[it]!! }.minOfOrNull { it } ?: 0.0

            paths.add(path to pathValue)
            path.edgeList.forEach { flowMapCopy[it] = flowMapCopy[it]!! - pathValue }

            flowValue = flowMapCopy.filter { it.key.target == graph.target }.values.sum()
        }

        check(flowMapCopy.values.sum() < ToleranceDoubleComparator.DEFAULT_EPSILON) { "Something in decomposition was done wrong, not all flow was assigned :(" }

        return paths
    }

    private fun solveMinCostCirulationProblem(
        timeHorizont: Double,
        inflowRate: Double
    ): Map<Edge, Double> {
        val graph = this.graph.graph
        // Build helper structures
        val model = ExpressionsBasedModel()
        val incomingEdgesMap: Map<Node, MutableSet<Edge>> =
            graph.vertexSet().associateWith { graph.incomingEdgesOf(it).toMutableSet() }
        val outgoingEdgesMap: Map<Node, MutableSet<Edge>> =
            graph.vertexSet().associateWith { graph.outgoingEdgesOf(it).toMutableSet() }

        // Now add the special back edge
        val backEdge = Edge(UUID.randomUUID().toString(), -timeHorizont, inflowRate, this.graph.target, this.graph.source)
        outgoingEdgesMap[this.graph.target]!!.add(backEdge)
        incomingEdgesMap[this.graph.source]!!.add(backEdge)
        val edges = graph.edgeSet() + backEdge

        // Create the lp variables
        val flowVariables: Map<Edge, Variable> = edges.associateWith {
            model.addVariable().apply {
                lower(0)
                weight(it.transitTime)

                if (it.capacity != Double.POSITIVE_INFINITY) {
                    upper(it.capacity)
                }

                logger.trace {
                    "0 <= f(${it.source.label ?: it.source.id}, ${it.target.label ?: it.target.id})" + (if (it.capacity != Double.POSITIVE_INFINITY) " <= ${it.capacity}" else "") + " (Cost: ${it.transitTime})"
                }
            }
        }
        val flowVariablesReversed = flowVariables.entries.associate { (k, v) -> v to k }

        // No need for the skew symmetry since we have a DAG.

        // Cost circulation so all flow on all nodes is conserved
        graph.vertexSet().forEach { u ->
            val incoming = incomingEdgesMap[u]!!
            val outgoing = outgoingEdgesMap[u]!!

            model.addExpression().apply {
                incoming.forEach {
                    this.set(flowVariables[it], -1)
                }

                outgoing.forEach {
                    this.set(flowVariables[it], 1)
                }
            }.lower(0).upper(0)

            logger.trace {
                incoming.joinToString(separator = " + ") { "f(${it.source.label ?: it.source.id}, ${it.target.label ?: it.target.id})" } + " = " +
                    outgoing.joinToString(
                        separator = " + "
                    ) { "f(${it.source.label ?: it.source.id}, ${it.target.label ?: it.target.id})" }
            }
        }

        val result = model.minimise()

        if (result.state != Optimisation.State.OPTIMAL) return emptyMap()

        val resultValues = result.elements().map { it.doubleValue() }

        return resultValues.mapIndexed { idx, d ->
            flowVariablesReversed[model.getVariable(idx)]!! to d
        }.filter { it.first != backEdge }.toMap()
    }
}
