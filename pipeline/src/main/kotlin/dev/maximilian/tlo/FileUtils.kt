package dev.maximilian.tlo

import java.net.URL

object FileUtils {
    private fun getResource(resource: String): URL? =
        Thread.currentThread().contextClassLoader?.getResource(resource)
            ?: FileUtils.javaClass.classLoader?.getResource(resource)
            ?: ClassLoader.getSystemResource(resource)

    fun getResourceContent(resource: String): String? = getResource(resource)?.readText()
}
