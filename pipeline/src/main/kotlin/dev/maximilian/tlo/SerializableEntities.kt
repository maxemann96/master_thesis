package dev.maximilian.tlo

import kotlinx.serialization.Serializable

// Json Graph Schema version 2 is used:
// https://jsongraphformat.info/v2.0/json-graph-schema.json
// Only graphs without hyper edges are supported

@Serializable
data class SerializableGraphBase<GraphMetadata, NodeMetadata, EdgeMetadata>(
    val graph: SerializableGraph<GraphMetadata, NodeMetadata, EdgeMetadata>? = null,
    val graphs: List<SerializableGraph<GraphMetadata, NodeMetadata, EdgeMetadata>>? = null
) {
    public val graphList: List<SerializableGraph<GraphMetadata, NodeMetadata, EdgeMetadata>> = graph?.let { listOf(graph) } ?: graphs ?: emptyList()

    init {
        require(graph != null || graphs != null) { "Exactly one of \"graph\" or \"graphs\" key is expected. No one was given" }
        require(graph != null && graphs == null || graphs != null && graph == null) { "Exactly one of \"graph\" or \"graphs\" key is expected. Both were given" }
    }
}

@Serializable
data class SerializableGraph<GraphMetadata, NodeMetadata, EdgeMetadata>(
    val id: String? = null,
    val label: String? = null,
    val directed: Boolean? = true,
    val type: String? = null,
    val metadata: GraphMetadata? = null,
    val nodes: Map<String, SerializableNode<NodeMetadata>>? = null,
    val edges: Set<SerializableEdge<EdgeMetadata>>? = null
) {
    init {
        // Check each edge connects two known nodes
        val edgeNodes: Set<String> = edges?.map { listOf(it.source, it.target) }?.flatten()?.toSet() ?: emptySet()
        val missingNodes: Set<String> = edgeNodes - (nodes?.keys ?: emptySet())

        require(missingNodes.isEmpty()) {
            "The following nodes were connected by edges, but are not in the set of nodes: $missingNodes"
        }

        // Check that there are (un)directed edges in a(n) (un)directed graph
        require(edges?.map { it.directed }?.all { it == directed } ?: true) {
            "At least one edge is (un)directed while the graph is (not) directed"
        }
    }
}

@Serializable
data class SerializableNode<T>(
    val label: String? = null,
    val metadata: T? = null
)

@Serializable
data class SerializableEdge<T>(
    val id: String? = null,
    val source: String,
    val target: String,
    val relation: String? = null,
    val directed: Boolean? = true,
    val label: String? = null,
    val metadata: T? = null
)

@Serializable
data class SerializableEdgeInformation(
    val outCapacity: Double,
    val transitTime: Double
)

@Serializable
data class SerializableQuickestFlow(
    val inflow: Double,
    val decomposition: List<Pair<Double, Set<String>>>
) {
    val value: Double = decomposition.sumOf { it.first }
}
