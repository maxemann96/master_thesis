package dev.maximilian.tlo.generator

import dev.maximilian.tlo.Edge
import dev.maximilian.tlo.Node
import dev.maximilian.tlo.SingleSourceTargetGraph
import dev.maximilian.tlo.sha256Sum
import kotlin.random.Random

object ParallelLinkNetworkFactory {
    fun generateNetwork(n: Int, seed: Long = Random.nextLong()): SingleSourceTargetGraph {
        val random = Random(seed)

        return generateNetwork(n, (0 until n).map { random.nextDouble() }.toDoubleArray(), (0 until n).map { random.nextDouble() }.toDoubleArray())
    }

    fun generateNetwork(n: Int, capacities: DoubleArray, transitTimes: DoubleArray): SingleSourceTargetGraph {
        require(n > 0) { "Need at least one link from source to target" }
        require(capacities.size == n) { "Given capacities size and given n are not equal" }
        require(transitTimes.size == n) { "Given transit times size and given n are not equal" }

        val s = Node("parallel-link-s$n$capacities$transitTimes".sha256Sum(), "s", trafficLight = false)
        val t = Node("parallel-link-t$n$capacities$transitTimes".sha256Sum(), "t", trafficLight = false)

        val edges = (0 until n).map {
            val transitTime = transitTimes[it]
            val capacity = capacities[it]
            val id = "${s.id}${t.id}$transitTime$capacity".sha256Sum()

            Edge(
                id = id,
                transitTime = transitTime,
                capacity = capacity,
                source = s,
                target = t
            )
        }.toSet()

        return SingleSourceTargetGraph(
            id = "$n$capacities$transitTimes".sha256Sum(),
            label = "Parallel link network with $n links",
            nodes = setOf(s, t),
            edges = edges,
            source = s,
            target = t,
            inflowRate = null
        )
    }
}
