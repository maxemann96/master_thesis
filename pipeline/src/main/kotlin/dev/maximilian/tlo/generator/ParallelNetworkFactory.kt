package dev.maximilian.tlo.generator

import dev.maximilian.tlo.Edge
import dev.maximilian.tlo.Node
import dev.maximilian.tlo.SingleSourceTargetGraph
import dev.maximilian.tlo.sha256Sum
import java.util.UUID
import kotlin.random.Random

object ParallelNetworkFactory {
    fun generateNetwork(n: Int, seed: Long = Random.nextLong()): SingleSourceTargetGraph {
        val random = Random(seed)

        return generateNetwork(
            n,
            (0 until n).map { random.nextDouble() }.toDoubleArray(),
            (0 until n).map { random.nextDouble() }.toDoubleArray()
        )
    }

    fun generateNetwork(n: Int, capacities: DoubleArray, transitTimes: DoubleArray): SingleSourceTargetGraph {
        require(n > 0) { "Need at least one intermediate node" }
        require(capacities.size == n) { "Given capacities size and given n are not equal" }
        require(transitTimes.size == n) { "Given transit times size and given n are not equal" }

        val s = Node("parallel-s$n$capacities$transitTimes".sha256Sum(), "s", trafficLight = false)
        val t = Node("parallel-t$n$capacities$transitTimes".sha256Sum(), "t", trafficLight = false)

        val nx = (0 until n).map { Node("parallel-N_${it + 1}-$n$capacities$transitTimes".sha256Sum(), "N_${it + 1}", trafficLight = false) }

        val edges = nx.mapIndexed { index, node ->
            val transitTime = transitTimes[index]
            val capacity = capacities[index]
            val idStub = "${s.id}${t.id}$transitTime$capacity"

            listOf(
                Edge(
                    id = "l$idStub".sha256Sum(),
                    transitTime = transitTime / 2.0,
                    capacity = capacity / 2.0,
                    source = s,
                    target = node
                ),
                Edge(
                    id = "r$idStub".sha256Sum(),
                    transitTime = transitTime / 2.0,
                    capacity = capacity / 2.0,
                    source = node,
                    target = t
                )
            )
        }.flatten().toSet()

        return SingleSourceTargetGraph(
            id = UUID.randomUUID().toString(),
            label = "Parallel network with $n intermediate nodes",
            nodes = (nx + s + t).toSet(),
            edges = edges,
            source = s,
            target = t,
            inflowRate = null
        )
    }
}
