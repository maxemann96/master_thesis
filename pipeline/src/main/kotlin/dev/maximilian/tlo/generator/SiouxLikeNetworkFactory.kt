package dev.maximilian.tlo.generator

import dev.maximilian.tlo.Edge
import dev.maximilian.tlo.Node
import dev.maximilian.tlo.SingleSourceTargetGraph
import dev.maximilian.tlo.sha256Sum
import kotlin.random.Random

object SiouxLikeNetworkFactory {
    fun generateNetwork(seed: Long = Random.nextLong()): SingleSourceTargetGraph {
        val random = Random(seed)
        val s = Node("sioux-falls-like-network-s-$seed".sha256Sum(), "s", trafficLight = false)
        val t = Node("sioux-falls-like-network-t-$seed".sha256Sum(), "t", trafficLight = false)
        val vx = (0 until 7).map { generateNode("v_${it + 1}") }.toTypedArray()

        val edges = setOf(
            generateEdge(random, s, vx[0]),
            generateEdge(random, s, vx[1]),
            generateEdge(random, vx[0], vx[2]),
            generateEdge(random, vx[0], vx[3]),
            generateEdge(random, vx[1], vx[4]),
            generateEdge(random, vx[1], vx[2]),
            generateEdge(random, vx[2], t),
            generateEdge(random, vx[3], vx[2]),
            generateEdge(random, vx[3], vx[6]),
            generateEdge(random, vx[4], vx[5]),
            generateEdge(random, vx[5], t),
            generateEdge(random, vx[6], vx[5])
        )

        return SingleSourceTargetGraph(
            id = "sioux-falls-like-network-$seed",
            label = "Sioux falls like network",
            nodes = (vx + s + t).toSet(),
            edges = edges,
            source = s,
            target = t,
            inflowRate = null
        )
    }

    private fun generateEdge(random: Random, source: Node, target: Node) = Edge(
        id = "${source.id}${target.id}".sha256Sum(),
        source = source,
        target = target,
        transitTime = random.nextDouble(),
        capacity = random.nextDouble()
    )
}
