package dev.maximilian.tlo.generator

import dev.maximilian.tlo.Node
import java.util.UUID

internal fun generateNode(label: String, trafficLight: Boolean = false) = Node(
    id = UUID.randomUUID().toString(),
    label = label,
    trafficLight = trafficLight
)
