package dev.maximilian.tlo.generator

import dev.maximilian.tlo.Edge
import dev.maximilian.tlo.Node
import dev.maximilian.tlo.SingleSourceTargetGraph
import dev.maximilian.tlo.sha256Sum
import kotlin.math.min
import kotlin.random.Random

interface IGridNetworkFactory : IGraphGenerator {
    fun generateGraph(rows: Int, cols: Int): SingleSourceTargetGraph
}

class GridNetworkFactory(private val seed: Long) : IGridNetworkFactory {
    companion object : IGridNetworkFactory by GridNetworkFactory(Random.nextLong())

    private val random = Random(seed)

    override fun generateGraph(rows: Int, cols: Int): SingleSourceTargetGraph {
        require(rows >= 3) { "Minimum 3 rows needed" }
        require(cols >= 3) { "Minimum 3 columns needed" }

        val nodes = (0 until rows).map { r ->
            (0 until cols).map { c ->
                Node(
                    id = if (r == 0 && c == 0) "s" else if (r == rows - 1 && c == cols - 1) "t" else "grid-network-${rows}x$cols-$seed-$r-$c".sha256Sum(),
                    label = if (r == 0 && c == 0) "s" else if (r == rows - 1 && c == cols - 1) "t" else "v_${r + 1},${c + 1}",
                    trafficLight = (r + c == min(rows, cols) - 1) && r != 0 && r != rows - 1
                )
            }
        }.flatten().toSet()

        val nodeArray = nodes.toTypedArray()
        val nodeByGrid: (Int, Int) -> Node = { x: Int, y: Int ->
            require(x >= 0) { "Row needs to be greater or equal 0" }
            require(x < rows) { "Row not in bounds" }

            require(y >= 0) { "Column needs to be greater or equal 0" }
            require(y < cols) { "Column not in bounds" }

            nodeArray[x * cols + y]
        }

        val edges = (0 until rows).map { r ->
            (0 until cols).map { c ->
                val ret = mutableListOf<Edge>()

                if (r + 1 < rows) {
                    ret.add(
                        Edge(
                            id = "${nodeByGrid(r, c).id}-${nodeByGrid(r + 1, c).id}".sha256Sum(),
                            transitTime = random.nextDouble(1.0, 5.0),
                            capacity = random.nextDouble(1.0, 5.0),
                            source = nodeByGrid(r, c),
                            target = nodeByGrid(r + 1, c),
                            label = null
                        )
                    )
                }

                if (c + 1 < cols) {
                    ret.add(
                        Edge(
                            id = "${nodeByGrid(r, c).id}-${nodeByGrid(r, c + 1).id}".sha256Sum(),
                            transitTime = random.nextDouble(1.0, 5.0),
                            capacity = random.nextDouble(1.0, 5.0),
                            source = nodeByGrid(r, c),
                            target = nodeByGrid(r, c + 1),
                            label = null
                        )
                    )
                }

                ret
            }.flatten()
        }.flatten().toSet()

        val leftUpper = nodeByGrid(rows - 1, 0)
        val rightLower = nodeByGrid(0, cols - 1)

        return SingleSourceTargetGraph(
            id = "grid-network-${rows}x$cols-$seed",
            label = "Grid network with $rows rows and $cols columns and random capacities and transit times in [1, 5) (Seed: $seed)",
            nodes = nodes.filterNot { it == leftUpper || it == rightLower }.toSet(),
            edges = edges.filterNot { it.source == leftUpper || it.target == leftUpper || it.source == rightLower || it.target == rightLower }.toSet(),
            source = nodes.first(),
            target = nodes.last(),
            inflowRate = null
        )
    }
}
