package dev.maximilian.tlo

import dev.maximilian.tlo.generator.GridNetworkFactory
import org.jetbrains.exposed.sql.Database

fun main() {
    System.setProperty(org.slf4j.impl.SimpleLogger.DEFAULT_LOG_LEVEL_KEY, if (System.getenv("DEBUG") == "true") "DEBUG" else "INFO")
    System.setProperty(org.slf4j.impl.SimpleLogger.SHOW_DATE_TIME_KEY, "true")
    System.setProperty(org.slf4j.impl.SimpleLogger.DATE_TIME_FORMAT_KEY, "yyyy-MM-dd'T'HH:mm:ss Z")

    val nfcCli = requireNotNull(System.getenv("NFC_CLI")) { "Please specify the path to the NashFlowComputation cli.sh in the environment variable \"NFC_CLI\"" }
    val dbURL = requireNotNull(System.getenv("DB_URL")) { "Please specify the postgres database URL in the environment variable \"DB_URL\", example: \"jdbc:postgresql://127.0.0.1:16010/tlo?user=tlo&password=tlo&ssl=false&&reWriteBatchedInserts=true&ApplicationName=TLO\"" }
    val iterations = 10

    val db = Database.connect(dbURL)
    TLO(db, emptySet(), setOf(GridNetworkFactory.generateGraph(4, 4)), nfcCli, iterations).runPipeline()
}
