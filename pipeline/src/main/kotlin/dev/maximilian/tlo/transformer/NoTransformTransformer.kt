package dev.maximilian.tlo.transformer

import dev.maximilian.tlo.GraphTransformation
import dev.maximilian.tlo.SingleSourceTargetGraph
import dev.maximilian.tlo.TransformationType

class NoTransformTransformer(graph: SingleSourceTargetGraph) : GraphTransformer(graph) {
    public override val transformation by lazy {
        GraphTransformation(
            id = 0,
            type = TransformationType.NOTHING,
            description = "No transformation",
            payload = "{}"
        )
    }

    override fun transform(): SingleSourceTargetGraph {
        val (nodes, edges, source, target) = getNodeEdgeDeepCopy()

        return SingleSourceTargetGraph(
            id = "${graph.id}-no-transform",
            label = "${graph.label} (${transformation.description})",
            nodes = nodes,
            edges = edges,
            source = source,
            target = target,
            inflowRate = graph.inflowRate
        )
    }
}
