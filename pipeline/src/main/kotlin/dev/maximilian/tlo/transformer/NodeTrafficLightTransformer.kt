package dev.maximilian.tlo.transformer

import dev.maximilian.tlo.BipartiteTrafficLightGadget
import dev.maximilian.tlo.Edge
import dev.maximilian.tlo.GraphTransformation
import dev.maximilian.tlo.Node
import dev.maximilian.tlo.SingleSourceTargetGraph
import dev.maximilian.tlo.TransformationType
import dev.maximilian.tlo.sha256Sum
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.math.BigDecimal
import java.math.MathContext
import java.util.UUID
import kotlin.random.Random

class NodeTrafficLightTransformer private constructor(
    graph: SingleSourceTargetGraph,
    private val mutationNumber: Int,
    private val payload: Map<Edge, List<Double>>
) : GraphTransformer(graph) {
    public constructor(graph: SingleSourceTargetGraph, mutationNumber: Int) : this(graph, mutationNumber, calculatePayload(graph))

    public companion object {
        private fun calculatePayload(graph: SingleSourceTargetGraph) = graph.nodes.filter { it.trafficLight }.map {
            val incomingEdges = graph.graph.incomingEdgesOf(it)
            val outgoingEdges = graph.graph.outgoingEdgesOf(it)

            require(incomingEdges.size > 0 && outgoingEdges.size > 0) {
                "Can only insert traffic lights at nodes with incoming and outgoing edges"
            }

            require(incomingEdges.size > 1 && outgoingEdges.size > 1) {
                "A traffic light at this position would change nothing since the bipartite gadget is the same as the original graph (incoming or outgoing edges count is 1)"
            }

            incomingEdges.associateWith { e ->
                val incomingCapacity = e.capacity
                val outgoingEdgesCount = outgoingEdges.size

                randomCapacities(incomingCapacity, outgoingEdgesCount)
            }
        }.let {
            val result = mutableMapOf<Edge, List<Double>>()

            it.forEach { e -> result.putAll(e) }

            result
        }

        /**
         * Calculates a random distribution of capacities between 0 and the upper bound in a way that the sum of all is equal to the upper bound.
         * To do this, (count - 1) random values between 0 and upper bound are calculated first, then sorted.
         * This leads to (count - 1) points on a line between 0 and upper bound.
         * At last the distances from 0 to first point, from first to second, ..., from last point to upper bound are calculated.
         */
        internal fun randomCapacities(upper: Double, count: Int): List<Double> {
            require(upper > 0) { "Upper value $upper needs to be greater than 0" }

            return when (count) {
                0 -> emptyList()
                1 -> listOf(upper)
                else -> (0 until count - 1)
                    .map { Random.nextDouble(upper).toBigDecimal().round(MathContext(5)) }
                    .toSortedSet()
                    .toTypedArray()
                    .let {
                        it.mapIndexed { index: Int, d: BigDecimal ->
                            d - if (index == 0) BigDecimal.ZERO else it[index - 1]
                        } + listOf(BigDecimal(upper, MathContext(5)) - it.last())
                    }.map { it.toDouble() }
            }
        }

        private val json = Json {
            allowSpecialFloatingPointValues = true
        }

        public fun fromDatabasePayload(graph: SingleSourceTargetGraph, payload: String): NodeTrafficLightTransformer {
            val asDbPayload = json.decodeFromString<DatabasePayload>(payload)

            return NodeTrafficLightTransformer(
                graph,
                asDbPayload.mutationNumber,
                asDbPayload.bipartiteEdgeCapacityMap.let { p ->
                    p.mapKeys { e1 -> graph.edges.single { e2 -> e2.id == e1.key } }
                }
            )
        }
    }

    override val transformation: GraphTransformation by lazy {
        GraphTransformation(
            id = 0,
            type = TransformationType.TRAFFIC_LIGHT_RANDOM,
            description = "Traffic lights with random distribution of edge capacities ($mutationNumber)",
            payload = json.encodeToString(DatabasePayload(mutationNumber, payload.mapKeys { it.key.id }))
        )
    }

    override fun transform(): SingleSourceTargetGraph {
        val (nodes, edges, source, target) = getNodeEdgeDeepCopy()
        val workingCopy = SingleSourceTargetGraph(UUID.randomUUID().toString(), "", nodes, edges, source, target, graph.inflowRate)

        workingCopy.nodes.filter { it.trafficLight }.map {
            generateTrafficLight(it, workingCopy)
        }.toSet()

        return SingleSourceTargetGraph(
            id = "${graph.id}-$mutationNumber",
            label = "${graph.label} (${transformation.description})",
            nodes = workingCopy.graph.vertexSet().toSet(),
            edges = workingCopy.graph.edgeSet().toSet(),
            source = workingCopy.source,
            target = workingCopy.target,
            inflowRate = workingCopy.inflowRate
        )
    }

    private fun generateTrafficLight(node: Node, workingCopy: SingleSourceTargetGraph): BipartiteTrafficLightGadget {
        val g = workingCopy.graph
        val incomingEdges = g.incomingEdgesOf(node).toSet()
        val outgoingEdges = g.outgoingEdgesOf(node).toSet()

        // First remove all these edges and the node
        g.removeAllEdges(incomingEdges + outgoingEdges)
        g.removeVertex(node)

        val incoming = incomingEdges.associateWith {
            Node(
                // Ensure that each time the traffic light is generated with same inputs, the same output is produced
                "${it.source.originalId}_${it.target.originalId}".sha256Sum(),
                "(${it.source.originalId}, ${it.target.originalId})",
                node.id,
                // After transform the traffic light gadget is built into the graph and thus no more traffic light flag
                false
            )
        }
        val outgoing = outgoingEdges.associateWith {
            Node(
                // Ensure that each time the traffic light is generated with same inputs, the same output is produced
                "${it.source.originalId}_${it.target.originalId}".sha256Sum(),
                "(${it.source.originalId}, ${it.target.originalId})",
                node.id,
                // After transform the traffic light gadget is built into the graph and thus no more traffic light flag
                false
            )
        }

        val newEdgeSet = mutableSetOf<Edge>()

        // Add to old incoming edge to the new node
        incoming.forEach {
            g.addVertex(it.value)

            val edge = it.key.copy(target = it.value)
            g.addEdge(it.key.source, it.value, edge)
            g.setEdgeWeight(edge, edge.transitTime)
            newEdgeSet.add(edge)
        }

        // Add to old outgoing edge from the new node
        outgoing.forEach {
            g.addVertex(it.value)

            val edge = it.key.copy(source = it.value)
            g.addEdge(it.value, it.key.target, edge)
            g.setEdgeWeight(edge, edge.transitTime)
            newEdgeSet.add(edge)
        }

        // Create the bipartite connection
        incoming.forEach { (edge, s) ->
            val randomNumberList = payload[edge]!!

            outgoing.values.forEachIndexed { idxY, t ->
                val newEdge = Edge(UUID.randomUUID().toString(), 0.0, randomNumberList[idxY], s, t)
                g.addEdge(s, t, newEdge)
                g.setEdgeWeight(newEdge, newEdge.transitTime)
            }
        }

        return BipartiteTrafficLightGadget(
            oldNode = node,
            newNodesL = incoming.values.toSet(),
            newNodesR = outgoing.values.toSet(),
            bipartiteEdges = newEdgeSet
        )
    }

    @Serializable
    private data class DatabasePayload(
        val mutationNumber: Int,
        val bipartiteEdgeCapacityMap: Map<String, List<Double>>
    )
}
