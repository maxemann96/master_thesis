package dev.maximilian.tlo.transformer

import dev.maximilian.tlo.Edge
import dev.maximilian.tlo.GraphTransformation
import dev.maximilian.tlo.Node
import dev.maximilian.tlo.SingleSourceTargetGraph

abstract class GraphTransformer(val graph: SingleSourceTargetGraph) {
    abstract val transformation: GraphTransformation
    abstract fun transform(): SingleSourceTargetGraph

    protected fun getNodeEdgeDeepCopy(): NodeEdgeDeepCopy {
        val newNodesByOldNodes = graph.nodes.associateWith { it.copy() }
        val newEdges = graph.edges.map {
            it.copy(
                source = newNodesByOldNodes[it.source]!!,
                target = newNodesByOldNodes[it.target]!!
            )
        }

        return NodeEdgeDeepCopy(
            nodes = newNodesByOldNodes.values.toSet(),
            edges = newEdges.toSet(),
            source = newNodesByOldNodes[graph.source]!!,
            target = newNodesByOldNodes[graph.target]!!
        )
    }

    protected data class NodeEdgeDeepCopy(
        val nodes: Set<Node>,
        val edges: Set<Edge>,
        val source: Node,
        val target: Node
    )
}
