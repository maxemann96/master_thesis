package dev.maximilian.tlo.transformer

import dev.maximilian.tlo.BipartiteTrafficLightGadget
import dev.maximilian.tlo.Edge
import dev.maximilian.tlo.Node
import dev.maximilian.tlo.SingleSourceTargetGraph
import dev.maximilian.tlo.addEdge
import dev.maximilian.tlo.sha256Sum
import java.util.UUID

abstract class TrafficLightTransformer(graph: SingleSourceTargetGraph) : GraphTransformer(graph) {
    private fun generateTrafficLight(
        node: Node,
        workingCopy: SingleSourceTargetGraph,
        capacitySupplier: (Node, Node) -> Double
    ): BipartiteTrafficLightGadget {
        val g = workingCopy.graph
        val incomingEdges = g.incomingEdgesOf(node).toSet()
        val outgoingEdges = g.outgoingEdgesOf(node).toSet()

        // Remove node inclusive edges
        require(g.removeVertex(node)) { "Cannot remove node from graph" }

        val incoming = incomingEdges.associateWith {
            Node(
                // Ensure that each time the traffic light is generated with same inputs, the same output is produced
                "${it.source.originalId}_${it.target.originalId}".sha256Sum(),
                "(${it.source.originalId}, ${it.target.originalId})",
                node.id,
                // After transform the traffic light gadget is built into the graph and thus no more traffic light flag
                false
            )
        }
        val outgoing = outgoingEdges.associateWith {
            Node(
                // Ensure that each time the traffic light is generated with same inputs, the same output is produced
                "${it.source.originalId}_${it.target.originalId}".sha256Sum(),
                "(${it.source.originalId}, ${it.target.originalId})",
                node.id,
                // After transform the traffic light gadget is built into the graph and thus no more traffic light flag
                false
            )
        }

        (incoming + outgoing).forEach { require(g.addVertex(it.value)) { "Cannot insert new node into graph" } }

        (incoming.map { it.key.copy(target = it.value) } + outgoing.map { it.key.copy(source = it.value) }).forEach { g.addEdge(it) }

        // Create the bipartite connection
        val bipartiteEdges = incoming.values.map { s ->
            outgoing.values.map { t ->
                val capacity = capacitySupplier(s, t)

                // Ensure that each time the traffic light is generated with same inputs, the same output is produced
                Edge("${s.id}${t.id}$capacity".sha256Sum(), 0.0, capacity, s, t).also { g.addEdge(it) }
            }
        }.flatten().toSet()

        return BipartiteTrafficLightGadget(
            oldNode = node,
            newNodesL = incoming.values.toSet(),
            newNodesR = outgoing.values.toSet(),
            bipartiteEdges = bipartiteEdges
        )
    }

    protected fun transformTrafficLights(
        id: String,
        label: String,
        capacitySupplier: (Node, Node) -> Double
    ): Pair<SingleSourceTargetGraph, Set<BipartiteTrafficLightGadget>> {
        val (nodes, edges, source, target) = getNodeEdgeDeepCopy()
        val workingCopy = SingleSourceTargetGraph(UUID.randomUUID().toString(), "", nodes, edges, source, target, graph.inflowRate)

        val bipartiteEdgeTrafficLightSet = workingCopy.nodes.filter { it.trafficLight }.map {
            generateTrafficLight(it, workingCopy, capacitySupplier)
        }.toSet()

        return SingleSourceTargetGraph(
            id = id,
            label = label,
            nodes = workingCopy.graph.vertexSet().toSet(),
            edges = workingCopy.graph.edgeSet().toSet(),
            source = workingCopy.source,
            target = workingCopy.target,
            inflowRate = workingCopy.inflowRate
        ) to bipartiteEdgeTrafficLightSet
    }
}
