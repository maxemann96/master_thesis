package dev.maximilian.tlo.transformer

import dev.maximilian.tlo.Edge
import dev.maximilian.tlo.GraphTransformation
import dev.maximilian.tlo.Node
import dev.maximilian.tlo.SingleSourceTargetGraph
import dev.maximilian.tlo.TransformationType
import java.util.UUID

class ShortestPathTrafficLightTransformer(graph: SingleSourceTargetGraph) : TrafficLightTransformer(graph) {
    override val transformation: GraphTransformation by lazy {
        GraphTransformation(
            id = 0,
            type = TransformationType.TRAFFIC_LIGHT_SHORTEST_PATH,
            description = "Shortest path traffic light transformation",
            payload = "{}"
        )
    }

    private val edgeMap = run {
        val (workingCopy, bipartiteEdgeTrafficLightSet) = transformTrafficLights(UUID.randomUUID().toString(), UUID.randomUUID().toString()) { _, _ -> 0.0 }
        val edgeMap = mutableMapOf<Edge, Double>()

        // For each gadget, calculate the capacities
        bipartiteEdgeTrafficLightSet.forEach { gadget ->
            // For each node on the right side, calculate the shortest path to target
            val rNodesByShortestPathToTarget: List<Node> = gadget
                .newNodesR
                .associateWith { workingCopy.getShortestPathToTarget(it) }
                .entries
                .sortedBy { it.value }
                .map { it.key }

            // For each left edge of the bipartite gadget
            // get the left node and split capacities
            // to nodes on the right side by shortest paths of the right side
            val lNodesByShortestPathFromSource = workingCopy.getShortestPathsFromSource()
                .filterKeys { n -> gadget.newNodesL.contains(n) }
                .entries
                .sortedBy { it.value }
                .map { it.key }

            val rCapacityFree = rNodesByShortestPathToTarget.associateWith { r ->
                val outgoingEdges = workingCopy.graph.outgoingEdgesOf(r)
                require(outgoingEdges.size == 1)
                outgoingEdges.first().capacity
            }.toMutableMap()

            lNodesByShortestPathFromSource.forEach { l ->
                val incomingEdges = workingCopy.graph.incomingEdgesOf(l)
                require(incomingEdges.size == 1)
                var lCapacityFree = incomingEdges.first().capacity

                rNodesByShortestPathToTarget.forEach { r ->
                    val edge = requireNotNull(workingCopy.graph.getEdge(l, r))
                    val capacityFree = rCapacityFree[r]!!

                    // Is it possible to have more incoming / outgoing capacity?
                    if (capacityFree > 0 && lCapacityFree > 0) {
                        // Can we completely fill in or partially fill in?
                        if (capacityFree > lCapacityFree) {
                            edgeMap[edge] = lCapacityFree
                            rCapacityFree[r] = rCapacityFree[r]!! - lCapacityFree
                            lCapacityFree = 0.0
                        } else {
                            edgeMap[edge] = capacityFree
                            rCapacityFree[r] = 0.0
                            lCapacityFree -= capacityFree
                        }
                    }
                }
            }
        }
        edgeMap
    }

    override fun transform(): SingleSourceTargetGraph = transformTrafficLights(
        id = "${graph.id}-shortest-path-transform",
        label = "${graph.label} (${transformation.description})",
    ) { v, w ->
        edgeMap.entries.firstOrNull { it.key.source.id == v.id && it.key.target.id == w.id }?.value ?: 0.0
    }.first
}
