package dev.maximilian.tlo

import dev.maximilian.tlo.transformer.GraphTransformer
import org.jgrapht.Graph
import org.jgrapht.traverse.DepthFirstIterator
import kotlin.math.min

class GraphOptimizer(graph: SingleSourceTargetGraph) : GraphTransformer(graph) {
    private fun removeZeroCapacityEdges(graph: Graph<Node, Edge>) {
        // Get all edges with zero capacity and remove them
        graph.edgeSet().filter { it.capacity == 0.0 }.forEach { graph.removeEdge(it) }
    }

    private fun removeDeadEnds(graph: Graph<Node, Edge>, target: Node) {
        graph.vertexSet()
            .filter { DepthFirstIterator(graph, it).asSequence().none { n -> n == target } }
            .forEach { graph.removeVertex(it) }
    }

    private fun simplifySingleInSingleOutNodes(graph: Graph<Node, Edge>) {
        graph.vertexSet()
            .filter { graph.incomingEdgesOf(it).size == 1 && graph.outgoingEdgesOf(it).size == 1 }
            .forEach {
                val incomingEdge = graph.incomingEdgesOf(it).single()
                val outgoingEdge = graph.outgoingEdgesOf(it).single()

                graph.removeVertex(it)
                graph.addEdge(
                    incomingEdge.source, outgoingEdge.target,
                    Edge(
                        id = "${incomingEdge.id}_${outgoingEdge.id}",
                        transitTime = incomingEdge.transitTime + outgoingEdge.transitTime,
                        capacity = min(incomingEdge.capacity, outgoingEdge.capacity),
                        source = incomingEdge.source,
                        target = outgoingEdge.target,
                        label = "${incomingEdge.label ?: incomingEdge.id} ${outgoingEdge.label ?: outgoingEdge.id}"
                    )
                )
            }
    }

    override val transformation: GraphTransformation = GraphTransformation(
        id = 0,
        type = TransformationType.OPTIMIZE,
        description = "Optimized Transformation",
        payload = "{}"
    )

    override fun transform(): SingleSourceTargetGraph {
        val (nodes, edges, source, target) = getNodeEdgeDeepCopy()
        val workingCopy = SingleSourceTargetGraph(
            id = "dummy",
            label = "dummy",
            nodes = nodes,
            edges = edges,
            source = source,
            target = target,
            inflowRate = graph.inflowRate
        )

        removeZeroCapacityEdges(workingCopy.graph)
        removeDeadEnds(workingCopy.graph, graph.target)
        simplifySingleInSingleOutNodes(workingCopy.graph)

        return SingleSourceTargetGraph(
            id = "${graph.id}-optimized",
            label = "${graph.label} (${transformation.description})",
            nodes = workingCopy.graph.vertexSet(),
            edges = workingCopy.graph.edgeSet(),
            source = source,
            target = target,
            inflowRate = graph.inflowRate
        )
    }
}
