package dev.maximilian.tlo

import kotlinx.serialization.json.JsonNull
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.buildJsonObject
import mu.KotlinLogging
import org.jgrapht.alg.shortestpath.DijkstraShortestPath
import org.jgrapht.graph.DirectedWeightedMultigraph

class SingleSourceTargetGraph(
    public val id: String,
    public val label: String,
    val nodes: Set<Node>,
    val edges: Set<Edge>,
    val source: Node,
    val target: Node,
    inflowRate: Double?
) {
    private val logger = KotlinLogging.logger { }

    init {
        require(nodes.isNotEmpty()) { "Empty graphs are not permitted" }
        require(edges.isNotEmpty()) { "A graph with no edges is not allowed" }
        require(nodes.contains(source)) { "The source node needs to be part of the graph" }
        require(nodes.contains(target)) { "The target node needs to be part of the graph" }

        require(source != target) { "Source and target nodes needs to be different" }
        require(
            edges.map { listOf(it.source, it.target) }.flatten().toSet().minus(nodes).isEmpty()
        ) { "There were nodes in the edge set, which exists not in the graph" }

        require(edges.none { it.source == target }) { "Target node needs to have no outgoing edges" }
        require(edges.none { it.target == source }) { "Source node needs to have no ingoing edges" }

        // TODO require DAG and one strongly connected component
    }

    public val graph = DirectedWeightedMultigraph<Node, Edge>(null, null).apply {
        nodes.forEach { addVertex(it) }
        edges.forEach { addEdge(it.source, it.target, it); setEdgeWeight(it, it.transitTime) }
    }

    public val inflowRate: Double by lazy {
        val defaultValue: Double = graph.outgoingEdgesOf(source).sumOf { it.capacity }

        if (inflowRate == null) {
            logger.warn { "Inflow not defined, using sum of outgoing capacities $defaultValue" }
            defaultValue
        } else {
            inflowRate
        }
    }

    public fun getQuickestFlow(flow: Double): QuickestFlow = QuickestFlow(this, flow, inflowRate)

    public fun getShortestPathsFromSource(): Map<Node, Double> = getShortestPathsFrom(source)

    public fun getShortestPathsFrom(node: Node): Map<Node, Double> = DijkstraShortestPath(graph).getPaths(node).let {
        nodes.associateWith { n -> it.getWeight(n) }
    }

    public fun getShortestSTPath(): Double = getShortestPathToTarget(source)

    public fun getShortestPathToTarget(node: Node): Double = requireNotNull(getShortestPathsFrom(node)[target]) {
        "Target node of graph is not reachable from ${node.label ?: node.id}"
    }

    public fun asSerializableGraph(): SerializableGraph<JsonObject, JsonObject, JsonObject> = SerializableGraph(
        id = id,
        directed = true,
        type = null,
        label = label,
        metadata = buildJsonObject {
            put("inflowRate", JsonPrimitive(inflowRate))
            put("type", JsonPrimitive("general"))
            put("maximumNumberOfIntervals", JsonPrimitive(25))
        },
        nodes = nodes.associate {
            it.id to SerializableNode(
                label = it.label,
                metadata = buildJsonObject {
                    put("trafficLight", JsonPrimitive(it.trafficLight))
                }
            )
        },
        edges = edges.map {
            SerializableEdge(
                id = it.id,
                source = it.source.id,
                target = it.target.id,
                metadata = JsonObject(
                    mapOf(
                        "outCapacity" to JsonPrimitive(it.capacity),
                        "transitTime" to JsonPrimitive(it.transitTime),
                        "inCapacity" to JsonPrimitive(Double.POSITIVE_INFINITY),
                        "storage" to JsonPrimitive(Double.POSITIVE_INFINITY),
                        "TFC" to JsonObject(
                            mapOf("resettingEnabled" to JsonNull, "active" to JsonNull)
                        )
                    )
                )
            )
        }.toSet()
    )

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SingleSourceTargetGraph

        if (id != other.id) return false
        if (label != other.label) return false
        if (nodes != other.nodes) return false
        if (edges != other.edges) return false
        if (source != other.source) return false
        if (target != other.target) return false
        if (inflowRate != other.inflowRate) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + label.hashCode()
        result = 31 * result + nodes.hashCode()
        result = 31 * result + edges.hashCode()
        result = 31 * result + source.hashCode()
        result = 31 * result + target.hashCode()
        result = 31 * result + inflowRate.hashCode()
        return result
    }
}
