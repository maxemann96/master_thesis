package dev.maximilian.tlo

import dev.maximilian.tlo.database.GraphDatabase
import dev.maximilian.tlo.nfc.NfcComputation
import dev.maximilian.tlo.transformer.GraphTransformer
import dev.maximilian.tlo.transformer.NoTransformTransformer
import dev.maximilian.tlo.transformer.NodeTrafficLightTransformer
import dev.maximilian.tlo.transformer.ShortestPathTrafficLightTransformer
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.double
import kotlinx.serialization.json.jsonPrimitive
import mu.KotlinLogging
import org.jetbrains.exposed.sql.Database
import java.util.UUID

class TLO(
    db: Database,
    graphFiles: Set<String>,
    graphs: Set<SingleSourceTargetGraph>,
    nfcExecutable: String,
    private val iterations: Int
) {
    private val logger = KotlinLogging.logger { }
    private val graphDb = GraphDatabase(db)
    private val nfcComputation = NfcComputation(nfcExecutable)

    private val graphs: List<SingleSourceTargetGraph> = (
        graphFiles
            .asSequence()
            .map { requireNotNull(FileUtils.getResourceContent("$it.json")) { "File with name $it does not exist and can't be loaded" } }
            .map { Json.decodeFromString<SerializableGraphBase<JsonObject, JsonObject, JsonObject>>(it).graphList }
            .flatten()
            .map {
                val ser = it.toGraph()
                val id = it.id ?: UUID.randomUUID().toString()
                val nodes = ser.second
                val edges = ser.third

                SingleSourceTargetGraph(
                    id,
                    it.label ?: id,
                    nodes,
                    edges,
                    nodes.first { n -> n.id == "s" },
                    nodes.first { n -> n.id == "t" },
                    it.metadata?.get("inflow")?.jsonPrimitive?.double
                )
            }
            .toList() + graphs
        )
        .map { GraphOptimizer(it).transform() }

    fun runPipeline() {
        logger.info { "Inserting graphs to database" }
        graphs.forEach { graphDb.insertGraph(it) }
        graphs.forEach {
            logger.info { "Processing graph ${it.id} (${it.label})" }

            acceptMutation(NoTransformTransformer(it))

            if (it.nodes.any { n -> n.trafficLight }) {
                (0 until iterations).forEach { mutNo ->
                    acceptMutation(NodeTrafficLightTransformer(it, mutNo))
                }
            }
        }
    }

    fun rerunPipelineStep(transformation: Int, overrideBound: Double? = null) {
        val (graph, mutation) = requireNotNull(graphDb.getGraphTransformation(transformation)) {
            "Transformation with id \"$transformation\" not found"
        }

        val transformer = when (mutation.type) {
            TransformationType.NOTHING -> NoTransformTransformer(graph)
            TransformationType.TRAFFIC_LIGHT_RANDOM -> NodeTrafficLightTransformer.fromDatabasePayload(graph, mutation.payload)
            TransformationType.TRAFFIC_LIGHT_SHORTEST_PATH -> ShortestPathTrafficLightTransformer(graph)
            TransformationType.OPTIMIZE -> GraphOptimizer(graph)
        }

        val transformedGraph = GraphOptimizer(transformer.transform()).transform()
        val shortestSTPath = transformedGraph.getShortestSTPath()
        val nfcComputed = nfcComputation.calculateIntervalBoundaryLabels(transformedGraph) ?: return

        val usefulBound = overrideBound ?: if (nfcComputed.intervalBoundaries.size <= 2) {
            100.0
        } else {
            (nfcComputed.intervalBoundaries.dropLast(1).last() * transformedGraph.inflowRate * 5 / 4)
                .takeIf { it < nfcComputed.intervalBoundaries.last() } ?: nfcComputed.intervalBoundaries.last()
        }

        (0 until 100).map { d -> shortestSTPath + (usefulBound / 100 * d) }.map { d ->
            val result = Triple(d, nfcComputed.labelAtTime(d / transformedGraph.inflowRate, transformedGraph.target), transformedGraph.getQuickestFlow(d).timeHorizon)
            println(result.second / result.third)
        }

        /*graphDb.insertResults(
            mutation,
            (0 until 100).map { d -> shortestSTPath + (usefulBound / 100 * d) }.map { d ->
                Triple(d, nfcComputed.labelAtTime(d / transformedGraph.inflowRate, transformedGraph.target), transformedGraph.getQuickestFlow(d).timeHorizon)
            }
        )*/

        println(nfcComputed)
    }

    private fun acceptMutation(transformer: GraphTransformer) {
        val transformation = transformer.transformation
        logger.info { "Processing mutation ${transformation.description} from graph ${transformer.graph.id}" }

        val timeBefore = System.nanoTime()
        val transformedGraph = GraphOptimizer(transformer.transform()).transform()
        val shortestSTPath = transformedGraph.getShortestSTPath()
        val nfcComputed = nfcComputation.calculateIntervalBoundaryLabels(transformedGraph)
        val timeAfter = System.nanoTime()

        val dbMutation = graphDb.insertGraphTransformation(transformation, transformer.graph, timeAfter - timeBefore, nfcComputed?.intervalBoundaries?.size ?: -2)

        if (nfcComputed == null) return

        val usefulBound = if (nfcComputed.intervalBoundaries.size <= 2) {
            100.0
        } else {
            (nfcComputed.intervalBoundaries.dropLast(1).last() * transformedGraph.inflowRate * 5 / 4)
                .takeIf { it < nfcComputed.intervalBoundaries.last() } ?: nfcComputed.intervalBoundaries.last()
        }

        graphDb.insertResults(
            dbMutation,
            (0 until 100).map { d -> shortestSTPath + (usefulBound / 100 * d) }.map { d ->
                Triple(d, nfcComputed.labelAtTime(d / transformedGraph.inflowRate, transformedGraph.target), transformedGraph.getQuickestFlow(d).timeHorizon)
            }
        )
    }
}
