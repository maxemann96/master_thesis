package dev.maximilian.tlo.nfc

import dev.maximilian.tlo.Node
import dev.maximilian.tlo.SerializableGraph
import dev.maximilian.tlo.SingleSourceTargetGraph
import dev.maximilian.tlo.format.cytoscapeSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import mu.KotlinLogging
import java.nio.file.Files
import java.nio.file.Paths

public class NfcComputation(nfcExecutable: String) {
    private val logger = KotlinLogging.logger { }

    private companion object {
        private val json = Json {
            allowSpecialFloatingPointValues = true
        }
    }

    init {
        require(Files.exists(Paths.get(nfcExecutable)))
    }

    private val processBuilder = dev.maximilian.tlo.pb.ProcessBuilder(
        "bash",
        "-c",
        nfcExecutable,
        json = json
    )

    fun calculateIntervalBoundaryLabels(graph: SingleSourceTargetGraph): NfcInstance? =
        kotlin.runCatching {
            processBuilder.execute<SerializableGraph<JsonObject, JsonObject, JsonObject>, List<StaticFlowIntervalResult>>(
                cytoscapeSerializer(),
                graph.asSerializableGraph()
            ).takeIf { it.isNotEmpty() }?.let { result ->
                val nodesById = graph.nodes.associateBy { it.id }
                NfcInstance(graph, result.map { it.toStatusFlowInterval(graph, nodesById) })
            }
        }.onFailure { logger.warn(it) { "Error while calling nfc tool" } }.getOrNull()

    @Serializable
    private data class StaticFlowIntervalResult(
        val start: Double,
        val end: Double,
        val nodeLabels: Map<String, Double>,
        val edgeFlow: List<StaticFlowEdgeFlow>
    )

    @Serializable
    private data class StaticFlowEdgeFlow(
        val source: String,
        val target: String,
        val flow: Double
    )

    private fun StaticFlowIntervalResult.toStatusFlowInterval(graph: SingleSourceTargetGraph, nodesById: Map<String, Node>) = StaticFlowInterval(
        start = start,
        end = end,
        lDash = nodeLabels.mapKeys { (id, _) ->
            nodesById[id] ?: throw IllegalArgumentException("Node with id \"$id\" not found in graph")
        },
        xeDash = edgeFlow.associate {
            val source = nodesById[it.source]
                ?: throw IllegalArgumentException("Node with id \"${it.source}\" not found in graph")
            val target = nodesById[it.target]
                ?: throw IllegalArgumentException("Node with id \"${it.target}\" not found in graph")

            graph.graph.getEdge(source, target) to it.flow
        }
    )
}
