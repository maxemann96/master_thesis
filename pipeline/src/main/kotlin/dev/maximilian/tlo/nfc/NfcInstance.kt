package dev.maximilian.tlo.nfc

import dev.maximilian.tlo.Edge
import dev.maximilian.tlo.Node
import dev.maximilian.tlo.SingleSourceTargetGraph
import dev.maximilian.tlo.isSorted

class NfcInstance(
    graph: SingleSourceTargetGraph,
    staticIntervals: List<StaticFlowInterval>
) {
    public val intervalBoundaries: List<Double> = staticIntervals.map { listOf(it.start, it.end) }.flatten().apply {
        require(this.size > 1) { "At least one interval is needed" }
        require(this.isSorted()) { "Interval list if not sorted" }
        require(
            this.subList(1, this.size - 1).chunked(2).all { it[0] == it[1] }
        ) { "Interval boundaries are not seamless" }
    }.toSet().toList()

    private val lDash: Map<Node, List<Double>> = staticIntervals.map { it.lDash }.let {
        val result = mutableMapOf<Node, MutableList<Double>>()

        it.forEach { map -> map.forEach { (node, value) -> result.getOrPut(node) { mutableListOf() }.add(value) } }

        result
    }

    private val xeDash: Map<Edge, List<Double>> = staticIntervals.map { it.xeDash }.let {
        val result = mutableMapOf<Edge, MutableList<Double>>()

        it.forEach { map -> map.forEach { (node, value) -> result.getOrPut(node) { mutableListOf() }.add(value) } }

        result
    }

    private val precomputedL: Map<Node, List<Double>> by lazy {
        val shortestPathValues: Map<Node, Double> = graph.getShortestPathsFromSource()

        val nodes = graph.nodes
        val nodeLabelMap: Map<Node, MutableList<Double>> =
            nodes.associateWith { mutableListOf(shortestPathValues[it]!!) }

        // Last interval has infinity label
        staticIntervals.subList(0, staticIntervals.size - 1).forEach {
            it.lDash.forEach { (node, value) ->
                nodeLabelMap[node]!! += nodeLabelMap[node]!!.last() + (it.end - it.start) * value
            }
        }

        nodeLabelMap
    }

    init {
        require(intervalBoundaries.size > 1) { "At least one interval is needed" }
        require(intervalBoundaries.isSorted()) { "Interval list is not sorted" }
        require(lDash.all { it.value.all { d -> d >= 0 } }) { "All l' needs to be positive or 0" }
    }

    fun labelAtTime(time: Double, node: Node): Double {
        require(time >= intervalBoundaries.first()) { "Time is out of range of intervals" }
        require(time <= intervalBoundaries.last()) { "Time is out of range of intervals" }

        val intervalStartIndex = intervalBoundaries.indexOfFirst { it >= time } - 1
        val startTimeOfInterval = intervalBoundaries[intervalStartIndex]
        val lDashOfNode = lDash[node]?.get(intervalStartIndex) ?: throw IllegalArgumentException("$node not found")
        val lOfNode = precomputedL[node]?.get(intervalStartIndex) ?: throw IllegalArgumentException("$node not found")

        return lOfNode + (time - startTimeOfInterval) * lDashOfNode
    }
}
