package dev.maximilian.tlo.nfc

import dev.maximilian.tlo.Edge
import dev.maximilian.tlo.Node

data class StaticFlowInterval(
    val start: Double,
    val end: Double,
    val lDash: Map<Node, Double>,
    val xeDash: Map<Edge, Double>
) {
    init {
        require(start < end) { "Interval start needs to be lesser than interval end" }
    }
}
