package dev.maximilian.tlo.database

import dev.maximilian.tlo.GraphTransformation
import dev.maximilian.tlo.SerializableGraph
import dev.maximilian.tlo.SingleSourceTargetGraph
import dev.maximilian.tlo.TransformationType
import dev.maximilian.tlo.database.ResultTable.default
import dev.maximilian.tlo.format.cytoscapeSerializer
import dev.maximilian.tlo.toGraph
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.double
import kotlinx.serialization.json.jsonPrimitive
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.batchInsert
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update

internal class GraphDatabase(private val db: Database) {
    private companion object {
        private val json = Json {
            allowSpecialFloatingPointValues = true
        }
    }

    init {
        transaction(db) {
            SchemaUtils.createMissingTablesAndColumns(GraphTable, TransformationTable, ResultTable)
        }
    }

    fun getGraph(id: String): SingleSourceTargetGraph? = transaction(db) {
        GraphTable.select { GraphTable.id eq id }.singleOrNull()?.toSingleSourceTargetGraph(id)
    }

    private fun ResultRow.toSerializableGraph(): SerializableGraph<JsonObject, JsonObject, JsonObject> = json.decodeFromString(this[GraphTable.structure])
    private fun ResultRow.toSingleSourceTargetGraph(id: String): SingleSourceTargetGraph = toSerializableGraph().let {
        val ser = it.toGraph()
        val nodes = ser.second
        val edges = ser.third

        SingleSourceTargetGraph(
            id,
            it.label ?: id,
            nodes,
            edges,
            nodes.first { n -> n.id == "s" },
            nodes.first { n -> n.id == "t" },
            it.metadata?.get("inflow")?.jsonPrimitive?.double
        )
    }

    fun getGraphs(): Set<SingleSourceTargetGraph> = transaction(db) {
        GraphTable.selectAll().map { it.toSingleSourceTargetGraph(it[GraphTable.id].value) }.toSet()
    }

    fun updateCytoscapeGraph(graph: SingleSourceTargetGraph) {
        transaction(db) {
            addLogger(StdOutSqlLogger)
            GraphTable.update(where = { GraphTable.id eq graph.id }) {
                it[cytoscapeData] = json.encodeToString(cytoscapeSerializer(), graph.asSerializableGraph())
            }
        }
    }

    fun insertGraph(graph: SingleSourceTargetGraph) {
        deleteGraph(graph.id)

        transaction(db) {
            GraphTable.insert {
                it[this.id] = graph.id
                it[structure] = json.encodeToString(graph.asSerializableGraph())
                it[label] = graph.label
                it[cytoscapeData] = json.encodeToString(cytoscapeSerializer(), graph.asSerializableGraph())
            }
        }
    }

    private fun deleteGraph(id: String) {
        transaction(db) {
            ResultTable.deleteWhere {
                ResultTable.transformationId inList TransformationTable.select { TransformationTable.graphId eq id }
                    .map { it[TransformationTable.id] }
            }
            TransformationTable.deleteWhere { TransformationTable.graphId eq id }
            GraphTable.deleteWhere { GraphTable.id eq id }
        }
    }

    fun insertGraphTransformation(
        graphTransformation: GraphTransformation,
        graph: SingleSourceTargetGraph,
        consumedTime: Long,
        nfcIntervals: Int
    ): GraphTransformation = transaction(db) {
        val result = TransformationTable.insertAndGetId {
            it[graphId] = graph.id
            it[transformationType] = graphTransformation.type
            it[description] = graphTransformation.description
            it[payload] = graphTransformation.payload
            it[TransformationTable.consumedTime] = consumedTime
            it[TransformationTable.nfcIntervals] = nfcIntervals
        }

        graphTransformation.copy(id = result.value)
    }

    fun getGraphTransformation(id: Int): Pair<SingleSourceTargetGraph, GraphTransformation>? = transaction(db) {
        TransformationTable.select { TransformationTable.id eq id }.singleOrNull()?.let {
            val graph = getGraph(it[TransformationTable.graphId].value)

            requireNotNull(graph) { "Saved graph not found in database. Foreign key constraint broken?" }

            graph to GraphTransformation(
                id = id,
                type = it[TransformationTable.transformationType],
                description = it[TransformationTable.description],
                payload = it[TransformationTable.payload]
            )
        }
    }

    fun insertResults(graphTransformation: GraphTransformation, results: List<Triple<Double, Double, Double>>) {
        transaction(db) {
            ResultTable.batchInsert(results) {
                this[ResultTable.transformationId] = graphTransformation.id
                this[ResultTable.inflow] = it.first
                this[ResultTable.nashFlowTime] = it.second
                this[ResultTable.quickestFlowTime] = it.third
            }
        }
    }
}

private open class StringIdTable(name: String = "", columnName: String = "id") : IdTable<String>(name) {
    override val id: Column<EntityID<String>> = varchar(columnName, 255).entityId()
    override val primaryKey by lazy { super.primaryKey ?: PrimaryKey(id) }
}

private object GraphTable : StringIdTable("graphs") {
    val label: Column<String?> = varchar("label", 255).nullable()
    val structure: Column<String> = text("data")
    val cytoscapeData: Column<String> = text("cytoscape_data").default("")
}

private object TransformationTable : IntIdTable("transformations") {
    val graphId: Column<EntityID<String>> = reference("graph_id", GraphTable.id)
    val transformationType: Column<TransformationType> = enumeration("type", TransformationType::class)
    val description: Column<String> = varchar("description", 255)
    val payload: Column<String> = text("payload")
    val consumedTime: Column<Long> = long("consumed_time")
    val nfcIntervals: Column<Int> = integer("nfc_result_interval_count").default(-1)
}

private object ResultTable : Table("results") {
    val transformationId: Column<EntityID<Int>> = reference("transformation_id", TransformationTable.id)
    val inflow: Column<Double> = double("flow")
    val nashFlowTime: Column<Double> = double("nash_flow_time")
    val quickestFlowTime: Column<Double> = double("quickest_flow_time")

    override val primaryKey = PrimaryKey(transformationId, inflow)
}
