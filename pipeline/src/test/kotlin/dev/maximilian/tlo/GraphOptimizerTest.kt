package dev.maximilian.tlo

import dev.maximilian.tlo.generator.GridNetworkFactory
import dev.maximilian.tlo.generator.ParallelLinkNetworkFactory
import dev.maximilian.tlo.generator.ParallelNetworkFactory
import dev.maximilian.tlo.generator.SiouxLikeNetworkFactory
import kotlin.test.Test
import kotlin.test.assertEquals

class GraphOptimizerTest {
    @Test
    fun `Graph Optimizer delivers same results for all types of generated networks`() {
        val networks = setOf(ParallelLinkNetworkFactory.generateNetwork(5), ParallelNetworkFactory.generateNetwork(5), SiouxLikeNetworkFactory.generateNetwork(), GridNetworkFactory.generateGraph(4, 4))
        val optimized1 = networks.map { GraphOptimizer(it).transform() }.toSet()
        val optimized2 = networks.map { GraphOptimizer(it).transform() }.toSet()

        assertEquals(optimized1, optimized2)
    }
}
