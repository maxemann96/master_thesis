package dev.maximilian.tlo

import dev.maximilian.tlo.transformer.NodeTrafficLightTransformer
import kotlin.test.Test
import kotlin.test.assertEquals

class UtilTest {
    @Test
    fun test() {
        val count = 3
        val upperValue = 0.7
        val result = NodeTrafficLightTransformer.randomCapacities(upperValue, count)

        assertEquals(count, result.count())
        assertEquals(upperValue, result.sum(), 1e-5)
        println(result)
    }
}
