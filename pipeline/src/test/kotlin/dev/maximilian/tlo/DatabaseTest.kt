package dev.maximilian.tlo

import dev.maximilian.tlo.database.GraphDatabase
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.double
import kotlinx.serialization.json.jsonPrimitive
import org.jetbrains.exposed.sql.Database
import java.sql.DriverManager
import java.util.UUID
import kotlin.test.Test
import kotlin.test.assertNotNull

class DatabaseTest {
    private val dbName = UUID.randomUUID().toString()

    private val database =
        Database.connect(getNewConnection = { DriverManager.getConnection("jdbc:h2:mem:$dbName;DB_CLOSE_DELAY=-1") })

    private val graphDatabase = GraphDatabase(database)

    @Test
    fun `Insert and get graph works`() {
        val jsonNetwork = Json.decodeFromString<SerializableGraphBase<JsonObject, JsonObject, JsonObject>>(FileUtils.getResourceContent("sioux-adjusted.json")!!).graph!!
        val ser = jsonNetwork.toGraph()
        val id = jsonNetwork.id ?: UUID.randomUUID().toString()
        val nodes = ser.second
        val edges = ser.third

        val graph = SingleSourceTargetGraph(
            id,
            jsonNetwork.label ?: id,
            nodes,
            edges,
            nodes.first { n -> n.id == "s" },
            nodes.first { n -> n.id == "t" },
            jsonNetwork.metadata?.get("inflow")?.jsonPrimitive?.double
        )

        graphDatabase.insertGraph(graph)

        val loadedGraph = graphDatabase.getGraph(graph.id)

        assertNotNull(loadedGraph)
        assert(graph == loadedGraph)
    }
}
