package dev.maximilian.tlo

import dev.maximilian.tlo.generator.GridNetworkFactory
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals

class GridNetworkFactoryTest {
    @Test
    fun `Generated graphs`() {
        val seeds = (0 until 100).map { Random.nextLong() }

        val result1 = seeds.map { GridNetworkFactory(it).generateGraph(4, 4) }
        val result2 = seeds.map { GridNetworkFactory(it).generateGraph(4, 4) }

        assertEquals(result1, result2)
    }
}
