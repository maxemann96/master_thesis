# Thesis

This folder contains the sources of the thesis itself.

## Requirements

An installed _LaTeX_ distribution with _latexmk_ support. You can get one for

1. Linux: Install _texlive_ with your package manager
2. macOS: Install _MacTeX_ with homebrew: `brew install --cask mactex`
3. Windows: Install [_MiKTeX_](https://miktex.org/)

## Build

```sh
latexmk --pdf thesis.tex
```
