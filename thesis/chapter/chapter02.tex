\chapter{Preliminaries}
\label{chap:preliminaries}
This chapter first introduces basic structures of graphs and road networks. These are built upon in the remainder of this thesis. Then dynamic flows and the optimal solution in the form of \emph{quickest flows} are presented. At the end of the chapter the different classes of graphs considered are discussed.

\section{Graphs and Flow Networks}
\begin{definition}[Directed graph]
    A directed graph $G = (V, A)$ consists of a finite node set $V$ and an arc set $A \subseteq V \times V \setminus \{ (v, v) \mid v \in V \}$. No loops or parallel arcs are allowed with this definition. To note the set of incoming arcs into a node $v \in V$, we use $\delta^-_v = \{ a = (v, w) \mid a \in A \}$. Analogously we note with $\delta^+_v = \{ a = (w, v) \mid a \in A \}$ the set of outgoing arcs. The head of an arc $a = (u, v) \in A$ is defined as $head(a) = v$ and the tail as $tail(a) = u$.
\end{definition}
Roads and intersections can be interpreted as a graph $G = (V, A)$. Each intersection is then be represented by a node $v \in V$ and each road connecting the intersections $u \in V$ and $w \in V$ is then be represented as the arc $(u, w) \in A$.

\begin{definition}[Node induced subgraph]
    Let $G = (V, A)$ be a graph and let $S \subset V$ be a subset of nodes from $V$. Then $G[S] = (S, \tilde{A})$ with arc set $\tilde{A} = A \cap \{ (v, w) \mid v,w \in S\}$ is the node induced subgraph from $G$. It is the graph which is created by deleting all nodes of $G$ and their adjacent arcs that are not in $S$.
\end{definition}

\begin{definition}[Path and Cycle]
    A path is a tuple of nodes connected neighborly with arcs between them. More formally a path $P$ with length $m$ is defined as $(v_0, \dots, v_m)$ with for all $i \in \{0, m - 1\}$ there exists an arc $(v_i, v_{i + 1}) \in A$. A cycle is a path where the first and the last node are the same, more formally $v_0 = v_m$. A graph is acyclic if it contains no cycle. A node $u \in V$ is reachable from a node $v \in V$ if there exists a path $(v, \dots, u)$.
\end{definition}

\begin{definition}[Network]
    \label{def:network}
    A network $\mathcal{N}$ is a graph $G = (V, A)$ extended by a single source $s$, a single target $t$, a capacity function $\nu \colon A \rightarrow \mathbb{R}_{\geq 0}$, a transit time function $\tau \colon A \rightarrow \mathbb{R}_{\geq 0}$ and a constant inflow rate $\nu_0 \in \mathbb{R}_+$. It is denoted by $\mathcal{N} = (G = (V, A), \nu, s, t, \tau, \nu_0)$. We call all nodes that are not the source and not the target node intermediate nodes, i.e. $V \setminus \{s, t\}$ is the set of intermediate nodes.
\end{definition}

\begin{definition}[Static flow]
    Let $\mathcal{N} = (G = (V, A), \nu, s, t, \tau, \nu_0)$ be a network with a always positive capacity function $\nu$, i.e. $\nu_a > 0$ for all $a \in A$. We call a vector $x = (x_a)_{a \in A}$ of arc flow values $x_a$ a static flow if it satisfies the flow conservation constraint for all $v \in V \setminus \{s, t\}$, i.e. that the accumulated incoming flow is equal to the accumulated outgoing flow on all nodes except $s$ and $t$. This ensures that all flow that enters an intermediate node also leaves it.

    \begin{align*}
        \sum_{a \in \delta^+_v} x_a = \sum_{a \in \delta^-_v} x_a
    \end{align*}

    The value $|x|$ of a flow is the outgoing flow minus the incoming flow from $s$ or $t$.

    \begin{align*}
        |x| \coloneqq \sum_{a \in \delta^+_s} x_a - \sum_{a \in \delta^-_s} x_a = \sum_{a \in \delta^-_t} x_a - \sum_{a \in \delta^+_t} x_a \geq 0
    \end{align*}

    A flow is called feasible if the capacity restriction holds for all arcs $a \in A$, i.e. that the flow on an arc is bounded by the capacity of the arc.

    \begin{align*}
        x_a \leq \nu_a
    \end{align*}

    An example of a static flow can be found in \Cref{fig:simple_flow_network}.
\end{definition}

A road network with cars or another type of moving vehicles or persons can be represented as the network defined above. As explained before, the intersections and roads can be interpreted as graphs. Then $s$ is the intersection where vehicles start to move and $t$ is the intersection where all vehicles have their destination.
\newpage
The flow conservation constraint ensures that no vehicle enters the network on any other point than the source and does not leave the network on any other point than the target. The capacity function $\nu$ is an abstraction of the real capacity of the road which can be seen as a variable correlating with the width of the road. The length of the road however correlates with the transit time function $\tau$. Things like road quality and speed limits can also be part of $\nu$ and $\tau$, but this would exceed the scope of this thesis.

\begin{figure}
    \begin{centering}
        \begin{tikzpicture}[shorten >=1pt,node distance=3cm,on grid,auto]
            \tikzstyle{vertex}=[circle,fill=black!25,minimum size=17pt,inner sep=0pt]

            \node[vertex] (s) at (0,0) {$s$};
            \node[vertex] (v1) [right of=s] {$v_1$};
            \node[vertex] (v2) [below of=s] {$v_2$};
            \node[vertex] (t) [right of=v2] {$t$};

            \path[->]
            (s)     edge [] node {$2/3, 2$} (v1)
            edge [] node[left] {$1/3, 1$} (v2)
            (v1)    edge [] node[above left] {$1/1, 3$} (v2)
            edge [] node {$1/3, 1$} (t)
            (v2)    edge [] node {$2/2, 2$} (t);
        \end{tikzpicture}

        \caption{An example of the graph $G = (V, A)$ with nodes $V = \{s, v_1, v_2, t\}$ and arcs $A = \{(s, v_1), (s, v_2), (v_1, v_2), (v_1, t), (v_2, t)\}$. The source of the flow network is $s$ and the target is $t$. On each arc $a \in A$ the capacity $\nu_a$, flow $f_a$ and the transit times $\tau_a$ are noted with $f_a / \nu_a, \tau_a$. The capacity function is defined as $\nu_{s, v_1} = 3$, $\nu_{s, v_2} = 3$, $\nu_{v_1, v_2} = 1$, $\nu_{v_1, t} = 3$, $\nu_{v_2, t} = 2$, the transit time function is defined as $\tau_{s, v_1} = 2$, $\tau_{s, v_2} = 1$, $\tau_{v_1, v_2} = 3$, $\tau_{v_1, t} = 1$, $\tau_{v_2, t} = 2$ and the flow is $x = (x_{s, v_1} = 2, x_{s, v_2} = 1, x_{v_1, v_2} = 1, x_{v_1, t} = 1, x_{v_2, t} = 2)$. The value of the flow is $|x| = 3$.}
        \label{fig:simple_flow_network}
    \end{centering}
\end{figure}

\section{Flows over Time}
Road traffic depends, among other things, on rush hours. This means that the flow at intersections and roads can vary greatly depending on the clock time. This is not sufficiently represented by the static flow model and for this reason we extend it to a dynamic model, called a flow over time or a dynamic flow.

\begin{definition}[Dynamic flow]
    Let $\mathcal{N} = (G = (V, A), \nu, s, t, \tau, \nu_0)$ be a network with $t$ is reachable from $s$. A locally integrable and bounded flow rate function $f_a \colon \mathbb{R} \Rightarrow \mathbb{R}_{\geq 0}$ for each $a \in A$ is given as well. This function returns the flow rate of arc $a$ for each point in time $\theta \in \mathbb{R}$. Then $f$ is defined as a dynamic flow on the network $\mathcal{N}$. A dynamic flow $f$ is called feasible if for almost all $\theta \in \mathbb{R}$ it fulfills the capacity constraints $f_a(\theta) \leq \nu_a$ for each arc $a \in A$. A dynamic flow $f$ fulfills strict flow conservation constraints if no flow is stored on intermediate nodes for almost all $\theta \in \mathbb{R}$. More formally:
    \begin{equation*}
        \sum_{a \in \varDelta^-(v)} \int_0^{\mathrlap{\theta - \tau_a}}f_a(\xi)d\xi = \sum_{a \in \varDelta^+(v)} \int_0^{\theta}f_a(\xi)d\xi
    \end{equation*}
\end{definition}

In this thesis we always look at dynamic flows that are feasible and fulfilling the strict flow conservation constraints. For a more comprehensive deeper dive into the various models of static and dynamic flows, refer to the summary of Skutella \cite{Skutella2009}, as above definitions were adapted from it.

\section{Quickest Flows}
\label{sec:qf}
To calculate a system optimum, quickest flows can be used. A quickest flow asks for a feasible dynamic flow fulfilling strict flow conservation constraints with maximum value $v = v(T)$ in a given time interval $[0, T]$. \cite{Burkard1993} described them first with discrete transmission times. Later \cite{Fleischer1998} extended them to the following definition.

\begin{definition}[Quickest flow]
    The quickest flow problem asks for a flow of value $v \in \mathbb{R}$ within a given time horizon $T$.
\end{definition}

The problem will be solved later by transforming the problem instance into an instance of the minimum cost circulation problem. Then the problem can be solved with a binary search in $[0, T]$. An implementation of this can be found in \Cref{sec:qf_solver}. To solve this with no upper bound for the time given, $T$ can be chosen as the time needed for sending all flow from source to target on the shortest path.

\section{Special Graph Classes}
\label{sec:graph_classes}
In the following we introduce some special graph classes which will be used in \Cref{chap:nash} and \Cref{chap:experiments}.

\begin{definition}[Parallel Network of degree $k$]
    \label{def:parallel_network}
    Given $n \in \mathbb{N}$, $k \in \mathbb{N}$ and $k \leq n$. A network $PG_{k, n} = (G, \nu, s, t, \tau, \nu_0)$ with $G = (V, A)$, node set $V = \{s, t, N_1, \dots, N_n\}$ and arc set $A = \{(s, N_i) \mid i \in \{1, \dots, n\}\} \cup \{(N_i, t) \mid i \in \{1, \dots, k\}\}$ is called a parallel network of degree $k$. With $k = n$ this is a parallel network as known. An example can be seen in \Cref{fig:parallel_degree}.
\end{definition}

\begin{figure}
    \begin{centering}
        \begin{tikzpicture}[shorten >=1pt,->]
            \node[draw, circle] (s) at (-2,0) {$s$};
            \node[draw, circle] (n1) at (4,1.5) {$N_1$};
            \node[draw, circle] (n2) at (4,.5) {$N_2$};
            \node[draw, circle] (n3) at (4,-.5) {$N_3$};
            \node[draw, circle] (n4) at (4,-1.5) {$N_4$};
            \node[draw, circle] (t) at (10,0) {$t$};

            \draw[->] (s) edge node[sloped, above right] {$\nu_1$, $\tau_{s, N_1}$}(n1);
            \draw[->] (s) edge node[sloped, above right] {$\nu_2$, $\tau_{s, N_2}$}(n2);
            \draw[->] (s) edge node[sloped, above right] {$\nu_3$, $\tau_{s, N_3}$}(n3);
            \draw[->] (s) edge node[sloped, above right] {$\nu_n$, $\tau_{s, N_4}$}(n4);

            \draw[->] (n1) edge node[sloped, above left] {$\nu_1$, $\tau_{N_1, t}$}(t);
            \draw[->] (n2) edge node[sloped, above left] {$\nu_2$, $\tau_{N_2, t}$}(t);
            \draw[->] (n3) edge node[sloped, above left] {$\nu_3$, $\tau_{N_3, t}$}(t);
        \end{tikzpicture}

        \caption{A parallel network of degree $k = 3$ with $n = 4$ intermediate nodes.}
        \label{fig:parallel_degree}
    \end{centering}
\end{figure}

Many cities are built with standard city layouts. In the following the grid network is outlined, which imitates the concept of blocks in the United States very well.

\begin{definition}[Grid network]
    \label{def:grid_network}
    Let $b, c \in \mathbb{N}$, $I \coloneqq \{1, \dots, b\}$ and $J \coloneqq 1, \dots, c\}$. Also let $V = \{v_{i, j} \mid i \in I, j \in J\}$ a node set and the set $A = \{\{(v_i, v_j), (v_{i'}, v_{j'})\} \mid i, i' \in I, j, j' \in J, |i - i'| + |j - j'| = 1\}$ a set of arcs. The network $\mathcal{N}_{b, c} = (G = (V, A), \nu, s = v_{1, 1}, t = v_{b, c}, \tau, \nu_0)$ is then called a grid network.
\end{definition}

\begin{definition}[Grid like network]
    \label{def:grid_like_network}
    Let $b, c \in \mathbb{N}$ and a grid network $\mathcal{N}_{b, c}$ be given. Also let $V_{\mathrm{corner}} = \{v_{b, 1}, v_{1, c}\}$ and $\tilde{G} = G[V - V_{\mathrm{corner}}]$ the node induced subgraph with the left upper and right lower corner cut out. Then $\tilde{\mathcal{N}}_{b, c}$ with the graph $\tilde{G}$ is a grid like network.
\end{definition}

In addition to laying out neighborhoods in blocks, cities are also crisscrossed and circumnavigated with highways and country roads. For this model, the city of Sioux Falls in the United States is presented in a simplified model. This deliberately omits the block style and concentrates only on the highways and rural roads. This was done because grid networks are considered separately above.

\begin{definition}[Sioux Falls like network]
    \label{def:sioux_network}
    Let $\mathcal{SX} = (G = (V, A), \nu, s, t, \tau, \nu_0)$ a network with $V, A, \nu, s, t, \tau$ and $\nu_0$ given in the graphical representation in \Cref{fig:sioux}. Then $\mathcal{SX}$ is a Sioux Falls like network, since it is a rough abstraction of the real city of Sioux Falls \cite{openstreetmapBounding}.
\end{definition}

\begin{figure}
    \begin{center}
        \begin{tikzpicture}
            \tikzstyle{vertex}=[circle,fill=black!25,minimum size=20pt,inner sep=0pt]

            \node[vertex] (v11) at (0,0) {$s$};
            \node[vertex] (v12) [right of=v11] {$v_{1, 2}$};
            \node[vertex] (v13) [right of=v12] {$v_{1, 3}$};

            \node[vertex] (v21) [above of=v11] {$v_{2, 1}$};
            \node[vertex] (v22) [right of=v21] {$v_{2, 2}$};
            \node[vertex] (v23) [right of=v22] {\underline{$v_{2, 3}$}};
            \node[vertex] (v24) [right of=v23] {$v_{2, 4}$};

            \node[vertex] (v31) [above of=v21] {$v_{3, 1}$};
            \node[vertex] (v32) [right of=v31] {\underline{$v_{3, 2}$}};
            \node[vertex] (v33) [right of=v32] {$v_{3, 3}$};
            \node[vertex] (v34) [right of=v33] {$v_{3, 4}$};

            \node[vertex] (v42) [above of=v32] {$v_{4, 2}$};
            \node[vertex] (v43) [right of=v42] {$v_{4, 3}$};
            \node[vertex] (v44) [right of=v43] {$t$};

            \path[->]
            (v11) edge (v12)
            (v12) edge (v13)

            (v21) edge (v22)
            (v22) edge (v23)
            (v23) edge (v24)

            (v31) edge (v32)
            (v32) edge (v33)
            (v33) edge (v34)

            (v42) edge (v43)
            (v43) edge (v44)

            (v11) edge (v21)
            (v12) edge (v22)
            (v13) edge (v23)

            (v21) edge (v31)
            (v22) edge (v32)
            (v23) edge (v33)
            (v24) edge (v34)

            (v32) edge (v42)
            (v33) edge (v43)
            (v34) edge (v44);
        \end{tikzpicture}
    \end{center}

    \caption{A grid like network with $b = c = 4$. For better readability transit times and capacities were omitted. The underlined nodes are the ones where the gadget from \Cref{def:tlg} is integrated during the experiments in \Cref{chap:experiments}.}
    \label{fig:grid}
\end{figure}

\begin{figure}
    \begin{center}
        \includegraphics[width=.45\textwidth]{images/osm_sioux_falls}\hspace{1em}\resizebox{.45\textwidth}{!}{\begin{tikzpicture}
                \tikzstyle{vertex}=[circle,fill=black!25,minimum size=20pt,inner sep=0pt]

                \node[vertex] (s) at (0, 0) {$s$};
                \node[vertex] (v1) at (1.5, 1.25) {$v_1$};
                \node[vertex] (v3) at (1.5, 4) {\underline{$v_3$}};
                \node[vertex] (v2) at (0, 4) {$v_2$};
                \node[vertex] (v4) at (3, 4) {$v_4$};
                \node[vertex] (v5) at (4.5, 4) {$v_5$};
                \node[vertex] (t) at (3, 6) {$t$};
                \node[vertex] (v6) at (4.5, 6) {$v_6$};
                \node[vertex] (v7) at (3, 8) {$v_7$};

                \draw[->] (v2) |- node[sloped, above] {$3, 3$} (v7);
                \draw[->] (v3) |- node[sloped, above] {$2, 4$} (t);
                \draw[->] (v6) |- node[sloped, above] {$3, 1.5$} (v7);

                \path[->]
                (s) edge node[sloped, below] {$3, 1$} (v1)
                (s) edge node[sloped, above] {$3, 1$} (v2)
                (v1) edge node[sloped, above] {$2, 2$} (v3)
                (v1) edge node[sloped, below] {$3, 1$} (v5)
                (v2) edge node[sloped, above] {$2, 2$} (v3)
                (v3) edge node[sloped, above] {$2, 2$} (v4)
                (v4) edge node[sloped, above] {$1, 3$} (t)
                (v5) edge node[sloped, above] {$2, 2$} (v4)
                (v5) edge node[sloped, above] {$3, 1$} (v6)
                (v6) edge node[sloped, above] {$1, 3$} (t)
                (v7) edge node[sloped, above] {$2, 2$} (t);
            \end{tikzpicture}}
    \end{center}

    \caption[Sioux Falls layout]{Sioux Falls layout to the left and the corresponding network to the right. Taken from OpenStreetMap \cite{openstreetmap} with bounding box $[-96.8019104004,43.4770895303,-96.68312072754,43.618057977924]$ and a scale of $100.000$. The underlined nodes are the ones where the gadget from \Cref{def:tlg} is integrated during the experiments in \Cref{chap:experiments}.}
    \label{fig:sioux}
\end{figure}
\newpage
\section{Game Theory}
To understand what a Nash equilibrium is we first need to introduce basic game theoretic aspects.

\begin{definition}[Games and Payoffs]
    Let $N$ be a set of players and $X_i$ a strategy set for each player $i \in N$. Then $N$ with $X_i$ and the following payoff function $P$ are forming a game.

    \begin{align*}
        P \colon X \rightarrow \mathbb{R}^N \qquad \mathrm{where}\quad X \coloneqq \bigtimes_{i \in N}X_i
    \end{align*}

    We define $x = (x_i)_{i \in N}$ as payoff strategy. We also define $P_i(x)$ as the payoff of player $i \in N$. With $(x_{-j}, y)$ we denote a strategy profile where $x_j$ is replaced by $y$ in the strategy profile $x = (x_i)_{i \in N \setminus \{y\}}$. As always in game theory, players act selfishly and try to maximize the own payoff.
\end{definition}

\begin{definition}[Nash equilibrium]
    A Nash equilibrium, denoted as $NE$, describes a balanced state of a game. No player can adjust her own behavior to achieve a better payoff. More formally, we define the strategy profile $x$ as a Nash equilibrium if the following holds:
    \begin{align*}
        P_i(x) \geq \max_{y \in X_i} P_i((x_{-i}, y))
    \end{align*}
\end{definition}

\begin{definition}[Price of Anarchy]
    As discussed, players act selfishly. This leads to results of the games that are inferior to the optimum. We call this factor the price of anarchy. We always assume the worst case, so more formally the following states the price of anarchy (PoA):
    \begin{align*}
        PoA \coloneqq \frac{\max_{x \in X} \sum_{i \in N}P_i(x)}{\min_{x \in NE} \sum_{i \in N}P_i(x)} \geq 1
    \end{align*}
\end{definition}


