# Master thesis

My [master thesis](https://gitlab.com/maxemann96/master_thesis/-/jobs/artifacts/main/raw/thesis/thesis.pdf?job=build:tex)

## Requirements

An installed _docker_ and _docker-compose_. You can get it for

1. Linux: Install it with the provided [documentation](https://docs.docker.com/engine/install/)
2. macOS: Install it with homebrew: `brew install --cask docker`
3. Windows: Install it with the provided [documentation](https://docs.docker.com/desktop/windows/install/)

## Checkout data

Possibility to view all the data in the browser and setup the whole stack locally:

```sh
docker-compose up -d
```

Then open a browser and go to [http://127.0.0.1:16012](http://127.0.0.1:16012)
An online version can be found [here](https://thesis.maximilian.dev)

## License

This work is licensed under multiple licenses, see [License.md](License.md) for more information.

## Contact

If you want to contact me, please write me a [mail](mailto:hello@maximilian.dev).
