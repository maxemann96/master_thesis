# Licenses

| File                                       | License                                                     |
| ------------------------------------------ | ----------------------------------------------------------- |
| thesis/images/logo.eps                     | Registered trademark of RWTH Aachen University              |
| thesis/images/osm_sioux_falls.jpg          | [ODbL](https://www.openstreetmap.org/copyright)             |
| All other files in thesis folder           | [Creative Commons CC-BY-NC-SA-4.0](LICENSE-CC-BY-NC-SA-4.0) |
| All other files excluding the license file | [Apache 2.0](LICENSE-Apache-2.0)                            |
