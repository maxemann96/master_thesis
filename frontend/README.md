# Frontend

This folder contains the sources of the frontend.

## Requirements

An installed _yarn_. You can get it for

1. Linux: Install _yarn_ with your package manager
2. macOS: Install it with homebrew: `brew install yarn`
3. Windows: Install it with the provided [installer](https://github.com/yarnpkg/yarn/releases)

## Setup

```sh
yarn install
```

## Local development serving

```sh
yarn serve
```

## Compiles and minifies for production

```sh
yarn build
```
