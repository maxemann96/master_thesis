import Vue from 'vue'
import App from './App.vue'
import ApolloClient from 'apollo-boost'
import VueApollo from 'vue-apollo'
import VueApexCharts from 'vue-apexcharts'
import vuetify from './plugins/vuetify'

Vue.config.productionTip = false

const apolloClient = new ApolloClient({
  // eslint-disable-next-line no-undef
  uri: (typeof API_BASE_URL === 'string') ? API_BASE_URL : "http://127.0.0.1:16011/v1/graphql"
})

Vue.use(VueApollo)
Vue.use(VueApexCharts)

Vue.component('apexchart', VueApexCharts)

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
})


new Vue({
  apolloProvider,
  vuetify,
  render: h => h(App)
}).$mount('#app')
